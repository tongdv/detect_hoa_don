@component('components.modals', [ 'idModal' => 'modal-auth-changepassword', 'title' => 'Thay đổi mật khẩu',
'buttonName' => 'Cập nhật' ] )
<form class="form-horizontal" action="{{ route('profile.changepassword') }}" method="POST" id={{"auth-changepassword-form"}}>
    {{ csrf_field() }}
    <div class="box-body">
        <div class="modal-body">
            <div class="row">
                <div class="form-group col-sm-12 col-xs-12">
                    <div class="row">
                        <label for="inputPassword" class="col-sm-4 control-label">Mật khẩu</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Mật khẩu hiện tại">
                        </div>
                    </div>
                </div>
                <div class="form-group col-sm-12 col-xs-12">
                        <div class="row">
                            <label for="inputNewPassword" class="col-sm-4 control-label">Mật khẩu mới</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" id="inputNewPassword" name="new_password" placeholder="Mật khẩu mới">
                            </div>
                        </div>
                    </div>
                <div class="form-group col-sm-12 col-xs-12">
                    <div class="row">
                        <label for="inputComfirmNewPassword" class="col-sm-4 control-label">Nhập lại mật khẩu mới</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputComfirmPassword" name="new_password_confirmation" placeholder="Nhập lại mật khẩu mới">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Cập nhật</button>
    </div>
</form>
@endcomponent