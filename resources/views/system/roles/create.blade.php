@component('components.modal-add', [ 
    'type' => 'role', 
    'title' => 'Thêm mới vai trò(role) người dùng',
    'width' => '25%',
    'route' => 'system.roles.add',
    ])
    <div class="row">
        <div class="col-sm-6">
            <label for="code" class="control-label">Mã</label>
            <input type="text" class="form-control" id="code" value="{{old('code')}}" name="code" placeholder="Mã quyền" autofocus tabindex="1" required>
        </div>
        <div class="col-sm-6">
            <label for="name" class="control-label">Tên</label>
            <input type="text" class="form-control" id="name" value="{{old('name')}}" name="name" placeholder="Vai trò(role)" tabindex="2" required>
        </div>
    </div>
    <div class="row">          
        <div class="col-sm-12">
            @component('components.group-checkbox', [
                'title' => 'Quyền hệ thống',
                'id' => 'system',
                'name' => 'system',
                'title_active' => 'Có',
                'title_inactive' => 'Không',
                'value_active' => 1,
                'value_inactive' => 0,
                'value' => old('active', 1),       
            ])
            @endcomponent

            <label for="description" class="control-label">Ghi chú</label>
            <input type="text" class="form-control" id="description" value="{{old('description')}}" name="description" placeholder="Mô tả chi tiết quyền" tabindex="3">
        </div>    
    </div>   
@endcomponent