@component('components.modal-update', [    
    'type' => 'update-user',
    'title' => 'Cập nhật thông tin người dùng',    
    'width' => '40%',
    'route' => 'users.update',
    'data' => $user,
    'method' => 'post' 
])

<div class="row">
    <div class="col col-md-6">
        <label for={{'inputName-detail-' . $user->id}} class="control-label">Tên</label>
        <input type="text" class="form-control" id={{'inputName-detail-' . $user->id}} value="{{$user->name}}" name="name" required>

        <label for={{'inputEmail-detail-' .$user->id}} class="control-label">Email</label>
        <input type="email" class="form-control" id={{'inputEmail-detail-' .$user->id}} value="{{$user->email}}" name="email" required>
              
        @component('components.group-checkbox', [
            'title' => 'Hoạt động',
            'id' => 'inputActive-detail-' . $user->id,
            'name' => 'active',
            'title_active' => 'Đang hoạt động',
            'title_inactive' => 'Ngừng hoạt động',
            'value_active' => 1,
            'value_inactive' => 0,
            'value' => $user->active,            
        ])
        @endcomponent             
    </div>
    <div class="col col-md-6">
        <label for={{'inputUserName-detail-' .$user->id}} class="control-label">Tài khoản</label>            
        <input type="text" class="form-control" id={{'inputUserName-detail-' .$user->id}} value="{{$user->username}}" name="username" required>
        
        <label for={{'inputRole-detail-' . $user->id}} class="control-label">Quyền</label>        
        @component('components.select', ['data' => $roles, 
            'text' => 'name', 
            'name' => 'role_id', 
            'value' => 'id',
            'id' => 'inputRole-detail-' . $user->id, 
            'idSelected' => $user->role->id])
        @endcomponent 

        <label for={{'inputPasword-detail-' . $user->id}} class="control-label">Mật khẩu mới</label>
        <input type="password" class="form-control" id={{'inputPasword-detail-' . $user->id}} name="password" placeholder="Mật khẩu hiện mới" minlength="6" maxlength="255">
    </div>                
</div>
@endcomponent