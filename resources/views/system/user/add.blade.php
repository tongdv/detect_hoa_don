@component('components.modal-add', [ 
    'type' => 'user', 
    'title' => 'Thêm mới người dùng',
    'width' => '40%',
    'route' => 'users.add',
    ])
    <div class="row">
        <div class="col-sm-6">
            <label for="name" class="control-label">Tên</label>
            <input type="text" class="form-control" id="name"  value="{{old('name')}}"  name="name" tabindex="1" autofocus required>           

            <label for="email" class="control-label">Email</label>
            <input type="email" class="form-control" id="email"  value="{{old('email')}}" name="email" tabindex="2" required> 
            
            @component('components.group-checkbox', [
                'title' => 'Hoạt động',
                'id' => 'active',
                'name' => 'active',
                'title_active' => 'Đang hoạt động',
                'title_inactive' => 'Ngừng hoạt động',
                'value_active' => 1,
                'value_inactive' => 0,
                'value' =>old('active',1),       
            ])
            @endcomponent   
        </div>
        <div class="col-sm-6">
            <label for="username" class="control-label">Tài khoản</label>            
            <input type="text" class="form-control"  value="{{old('username')}}" id="username" name="username" tabindex="3" required>
            
            <label for="role_id" class="control-label">Quyền</label>        
            @component('components.select', ['data' => $roles, 
                'text' => 'name', 
                'name' => 'role_id', 
                'value' => 'id',
                'id' => 'role_id', 
                'idSelected'=>  old('role_id'),
                'tabindex'=>4
                ])
            @endcomponent 
    
            <label for="password" class="control-label">Mật khẩu</label>
            <input type="password" class="form-control"  value="{{old('password')}}" id="password" name="password" tabindex="5" required minlength="6" maxlength="255">

            <label for="password_confirmation" class="control-label">Nhập lại mật khẩu</label>
            <input type="password" class="form-control"  value="{{old('password_confirmation')}}" id="password_confirmation" name="password_confirmation" tabindex="6" required minlength="6" maxlength="255">
        </div>
    </div>      
@endcomponent