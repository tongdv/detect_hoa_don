@component('components.box-search', ['routerName' => 'report.sumtotal', 'search' => (empty($search)? null: $search)])
    <div class="row">
        <div class="col-md-12">
            <label>Chọn tháng</label><br/>
            <div class="row">
                <div class="col-md-6">
                    <div class="input-group">
                        <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                        </div>
                    <input type="text" class="form-control monthmask" id="search_month" name="search_month" value="{{isset($search_month) ? $search_month: null}}">
                    </div>                    
                </div>                
            </div>

            @component('components.multiple-select', [
                'label' => 'Dịch vụ',
                'placeholder' => 'Chọn dịch vụ',
                'data' => $dichvus,
                'text' => 'name',
                'value' => 'id',
                'id' => 'search_dich_vu',
                'name' => 'search_dich_vu',
                'selected' => $search_dich_vu,
                'required' => false,
            ])
            @endcomponent            
        </div>        
    </div>
@endcomponent


