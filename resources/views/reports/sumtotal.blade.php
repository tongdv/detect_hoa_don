@extends('layouts.app') 


@section('css')
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('title')
    <h1>Báo cáo</h1>
@endsection

@section('content')
<div class="box">
    <div class="box-header">
        @component('components.alert')               
        @endComponent           
    </div>
    <div class="box-footer">
            <div class="row">
                <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">                   
                    <h5 class="description-header">{{number_format($total, 0)}}</h5>
                    <span class="description-text">TỔNG SỐ HÓA ĐƠN</span>
                </div>
                <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">                    
                    <h5 class="description-header">{{number_format($sum_total, 0)}}</h5>
                    <span class="description-text">TỔNG TIỀN</span>
                </div>
                <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">                    
                    <h5 class="description-header">{{number_format($total_service, 0)}}</h5>
                    <span class="description-text">SỐ LĨNH VỰC</span>
                </div>
                <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                <div class="description-block">                                        
                    @if($total > 0) 
                        <h5 class="description-header">{{number_format($sum_gtgt, 0)}}</h5>
                    @else
                        <h5 class="description-header">0</h5>
                    @endif
                    <span class="description-text">TỔNG TIỀN THUẾ GTGT</span>
                </div>
                <!-- /.description-block -->
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">                   
                    <h5 class="description-header">{{number_format($total_input, 0)}}</h5>
                    <span class="description-text">HÓA ĐƠN ĐẦU VÀO</span>
                </div>
                <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">                    
                    <h5 class="description-header">{{number_format($total - $total_input, 0)}}</h5>
                    <span class="description-text">HÓA ĐƠN ĐẦU RA</span>
                </div>
                <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">                    
                    <h5 class="description-header">{{number_format($total_validated, 0)}}</h5>
                    <span class="description-text">HÓA ĐƠN HỢP LỆ</span>
                </div>
                <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                <div class="description-block">                                        
                    @if($total > 0) 
                        <h5 class="description-header">{{number_format($total - $total_validated, 0)}}</h5>
                    @else
                        <h5 class="description-header">0</h5>
                    @endif
                    <span class="description-text">HÓA ĐƠN KHÔNG HỢP LỆ</span>
                </div>
                <!-- /.description-block -->
                </div>
            </div>
            <!-- /.row -->
        </div>
    <div class="box-body">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="row">  
                <div class="col-sm-2">
                    @component('components.perpage',['options' => [10,20,50,100], 'default'=> $data->perPage(), 'data' => $data, 'routerName' => 'report.sumtotal'])
                    @endComponent
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-flat bg-olive" id="btnXuatXml" onclick="downloadXml()">
                        <i class="fa fa-file-excel-o"> Xuất dữ liệu xml</i>
                    </button> 
                </div>            
                <div class="col-sm-8">
                    @include('reports.box-search')
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-condensed">
                        <thead>
                        <tr role="row">
                            <th style="width: 10%;">Hóa đơn</th>
                            <th style="width: 20%;">Lĩnh vực</th>
                            <th style="width: 10%;">Hợp lệ</th>
                            <th style="width: 10%;">Loại</th>                            
                            <th style="width: 100%;text-align:center">Tổng tiền</th>
                            <th style="width: 10%;text-align:center">GTGT</th>
                            <th style="width: 10%;text-align:center">Ngày tạo</th>
                            <th style="width: 10%;text-align:center">Người tạo</th>
                            <th style="width: 10%;text-align:center"></th>
                        </tr>
                        </thead>
                        <tbody>             
                            @foreach ($data as $order)
                                <tr role="row">
                                    <td>{{$order->code}}</td>
                                    <td>{{isset($order->service) ? $order->service->name : '---'}}</td>
                                    <td style="text-align:center;">
                                        @if ($order->validator)
                                            <small class="label label-success">Hợp lệ</small>
                                        @else
                                            <span class="label label-warning">Không hợp lệ</span>
                                        @endif                    
                                    </td>
                                    <td style="text-align:center;">
                                        @if ($order->input)
                                            <small class="label label-success">Đầu vào</small>
                                        @else
                                            <span class="label label-warning">Đầu ra</span>
                                        @endif                    
                                    </td> 
                                    <td style="text-align:center;">                                        
                                        <span class="badge bg-olive">{{ number_format($order->total, 0)}}</span>
                                    </td>
                                    <td style="text-align:center;">                                        
                                        <span class="badge bg-olive">{{ number_format($order->gtgt, 0)}}</span>
                                    </td>
                                    <td style="text-align:center;">
                                        {{ $order->created_at->format('d/m/Y')}}
                                    </td>
                                    <td style="text-align:center;">
                                        {{ $order->createdBy->name}}
                                    </td> 
                                    <td align="center">
                                        <a href="#" data-toggle="modal" data-target="{{ '#modal-detail-' . $order->id }}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        @include('order.detail', ['model' => $order, 'buttonName' => 'Cập nhật'])                   
                                    </td>                               
                                </tr>
                            @endforeach      
                        </tbody>                    
                    </table>
                </div>                
            </div>
            @component('components.pagination', ['pageShow' => 3, 'data' => $data])
            @endComponent           
        </div>                 
    </div>    
</div>
@endsection 

@section('script')    
    <script src="{{ asset('js/reports/downloadorderxml.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script> 
@endsection