@extends('layouts.app') 


@section('css')
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('title')
    <h1>Báo cáo thống kê
    </h1>
@endsection

@section('content')
<div class="box">
    <div class="box-header">
        @component('components.alert')               
        @endComponent           
    </div>
    <div class="box-body">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="row">                              
                <div class="col-md-8 col-md-offset-4">
                    @include('reports.inputoutput.box-search')
                </div>
            </div>                       
        </div>                 
    </div>

    <br/>
    <div class="box-footer">
            <div class="row">
                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">                   
                        <h5 class="description-header">{{number_format($total, 0)}}</h5>
                        <span class="description-text">TỔNG SỐ HÓA ĐƠN</span>
                    </div>
                    <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">                    
                        <h5 class="description-header">{{number_format($sum_total, 0)}}</h5>
                        <span class="description-text">TỔNG TIỀN</span>
                    </div>
                <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">                   
                        <h5 class="description-header">{{number_format($total_input, 0)}}</h5>
                        <span class="description-text">HÓA ĐƠN ĐẦU VÀO</span>
                    </div>
                    <!-- /.description-block -->
                </div>
                    <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">                    
                        <h5 class="description-header">{{number_format($total - $total_input, 0)}}</h5>
                        <span class="description-text">HÓA ĐƠN ĐẦU RA</span>
                    </div>
                    <!-- /.description-block -->
                </div>
            </div>
            <div class="row">
                
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">                    
                    <h5 class="description-header">{{number_format($total_validated, 0)}}</h5>
                    <span class="description-text">HÓA ĐƠN HỢP LỆ</span>
                </div>
                <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                <div class="description-block">                                        
                    @if($total > 0) 
                        <h5 class="description-header">{{number_format($total - $total_validated, 0)}}</h5>
                    @else
                        <h5 class="description-header">0</h5>
                    @endif
                    <span class="description-text">HÓA ĐƠN KHÔNG HỢP LỆ</span>
                </div>
                <!-- /.description-block -->
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">                    
                        <h5 class="description-header">{{number_format($sum_input, 0)}}</h5>
                        <span class="description-text">TỔNG ĐẦU VAO</span>
                    </div>
                    <!-- /.description-block -->
                </div>

                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">                    
                        <h5 class="description-header">{{number_format($sum_total - $sum_input, 0)}}</h5>
                        <span class="description-text">TỔNG ĐẦU RA</span>
                    </div>
                    <!-- /.description-block -->
                </div>
            </div>
            <!-- /.row -->
        </div> 
</div>
@endsection 

@section('script')        
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script> 
@endsection