@foreach ($menus as $menu)   
    @if (count($menu->children) > 0)
        <li class="treeview">
            <a href="{{(!Route::has($menu->router_link)) ? '#' : route($menu->router_link)}}"><i class="fa {{$menu->fa_icon}}"></i> <span>{{$menu->name}}</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>    
            <ul class="treeview-menu">                            
                @foreach ($menu->children as $menu_detail)
                    <li><a href={{(!Route::has($menu_detail->router_link)) ? '#' : route($menu_detail->router_link)}}><i class="fa {{$menu_detail->fa_icon}}"></i> {{$menu_detail->name}}</a></li>
                @endforeach
            </ul>
        </li>           
    @else
        <li><a href={{(!Route::has($menu->router_link)) ? '#' : route($menu->router_link)}}><i class="fa {{$menu->fa_icon}}"></i> <span>{{$menu->name}}</span></a></li>
    @endif    
@endforeach