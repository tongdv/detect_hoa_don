@component('components.confirm-delete',
[
    'route' => 'orders.delete',
    'method' => 'delete',
    'data' => $model,
    'type' => 'delete-order',
    'title' => 'Xóa hóa đơn'
])

<div class="row">
    <div class="form-group col-sm-12">
        <div class="row">
            <label for={{'inputName-delete-' . $model->id}} class="col-sm-3 control-label">Mã</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id={{'inputName-delete-' . $model->id}} value="{{$model->code}}" name="code" disabled>
            </div>
        </div>        
    </div>
</div>
@endcomponent