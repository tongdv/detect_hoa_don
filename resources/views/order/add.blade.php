@component('components.modals', [ 'idModal' => 'modal-add-order', 'title' => 'Thêm mới hóa đơn',
'buttonName' => 'Tạo mới' ] )

<form class="form-horizontal" action="{{ route('orders.addByDetect') }}" method="POST" id="order-add-form" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="x1" value="" />
    <input type="hidden" name="y1" value="" />
    <input type="hidden" name="x2" value="" />
    <input type="hidden" name="y2" value="" />
    <div class="box-body">
        <div class="modal-body">
            <div class="file-upload">
                <div class="image-upload-wrap">
                    <input class="file-upload-input" type='file' onchange="readURL(this);" accept="image/*" name="imageOrder" required/>                    
                    <div class="drag-text">
                        <h3>Tải ảnh hóa đơn</h3>
                    </div>
                </div>
                <div class="file-upload-content">
                    <img class="file-upload-image" src="#" alt="your image" id="file-upload-image"/>                    
                    <br/>
                    <br/>
                    <div class="image-title-wrap">
                        <button type="button" class="btn btn-success btn-flat" onclick="$('.file-upload-input').trigger( 'click' )"> Thay đổi ảnh</button>                        
                    </div>
                </div>
            </div> 
            
            @component('components.group-checkbox', [
                'title' => 'Loại hóa đơn',
                'id' => 'input',
                'name' => 'input',
                'title_active' => 'Đầu vào',
                'title_inactive' => 'Đầu ra',
                'value_active' => 1,
                'value_inactive' => 0,
                'value' =>old('input',1),       
            ])
            @endcomponent
        </div>       
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left btn-flat" data-dismiss="modal" tabindex="8"><i class="fa fa-close"></i> Đóng</button>
        <button type="submit" class="btn btn-primary btn-flat"tabindex="7" onclick="this.form.submit(); this.disabled=true;"><i class="fa fa-check"></i> Detect hóa đơn</button>
    </div>
</form>
@endcomponent