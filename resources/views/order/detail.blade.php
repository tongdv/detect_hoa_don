@component('components.modals', [ 'idModal' => 'modal-detail-'. $model->id, 'title' => 'Cập nhật hóa đơn'] )

<form class="form-horizontal" action="{{ route('orders.update', $model->id) }}" method="POST" id="order-add-form" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('put') }}
    <div class="box-body">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <img class="file-upload-image" src="{{ URL::to($model->url_image) }}" alt="your image" />
                </div>                
            </div>
            <br/>
            <div class="row">
                <label for="{{ 'inputTextDetect-detail-' . $model->id}}" class="control-label">Kết quả detect</label>
                <textarea row ="10" cols="50" class="form-control" id={{ 'inputTextDetect-detail-' . $model->id}} name="text_detect" tabindex="1">{{$model->text_detect}}</textarea>                
            </div> 
            <br/>
            <div class="row">
                <div class="col col-md-12">                    
                    <label for="{{ 'inputService-detail-' . $model->id}}">Dịch vụ</label>              
                    <select class="form-control" id="services" name="service_id">
                        <option value={{null}}>Tất cả</option>
                        @foreach($services as $item)                  
                            <option value={{$item->id}} {{(isset($model) && $item->id==$model->service_id) ? 'selected="selected"' : null}}>{{$item->name}}</option>
                        @endforeach              
                    </select>
                </div>
            </div> 
            
            <div class="row">
                <div class="col col-md-6">                    
                    @component('components.group-checkbox', [
                        'title' => 'Loại hóa đơn',
                        'id' => 'input',
                        'name' => 'input',
                        'title_active' => 'Đầu vào',
                        'title_inactive' => 'Đầu ra',
                        'value_active' => 1,
                        'value_inactive' => 0,
                        'value' => $model->input,       
                        ])
                    @endcomponent
                </div>
                <div class="col-md-6">
                    <label class="control-label">Tổng tiền</label>
                    <input type="text" class="form-control maskmoney" value="{{$model->total}}" name="total"> 
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left btn-flat" data-dismiss="modal" tabindex="8"><i class="fa fa-close"></i> Đóng</button>
        <button type="submit" class="btn btn-primary btn-flat"tabindex="7"><i class="fa fa-check"></i> Cập nhật</button>
    </div>
</form>
@endcomponent