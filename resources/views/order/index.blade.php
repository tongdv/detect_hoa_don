@extends('layouts.app') 

@section('css')
  <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
  <link rel="stylesheet" href="{{ asset('css/uploadimage.css') }}">
  <link rel="stylesheet" href="{{ asset('bower_components/imgareaselect/dist/imgareaselect.css') }}">
@endsection

@section('title')
  <h1>Hóa đơn</h1>
@endsection 

@section('content')
<div class="box">
  <div class="box-header">
      @component('components.alert')       
        @if ($errors->any())
          <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
          </ul>
        @endif
      @endComponent   
    <div class="row">     
      <div class="col-xs-12 pull-right">
        <a href="#" data-toggle="modal" data-target="{{ '#modal-add-order'}}" class="btn btn-default">
          <i class="fa fa-plus"> Thêm mới</i>
        </a>
        <a href="{{env('WEBSITE_THUE','http://tracuuhoadon.gdt.gov.vn/tc1hd.html')}}" target="_blank" class="btn btn-default">
            <i class="fa fa-share-alt"> Website của tổng cục thuế</i>
        </a>                   
        @include('order.add')       
      </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-md-3">
          @component('components.perpage',['options' => [10,20,50,100], 'default'=> $data->perPage(),'data' => $data, 'routerName' => 'orders'])
          @endComponent
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Thời gian</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <form id="formdatetimefilter" action={{ route('orders')}} method="GET">
                <input type="text" class="form-control pull-right" name="datefilter" value="{{empty($datefilter) ? null : $datefilter}}" onchange="this.form.submit()">                              
              </form>             
            </div>            
          </div>
        </div>                        
      </div>
      <br/>
      <div class="row"> 
        <div class="col col-md-6 col-sm-6 col-lg-6">
          <form id="form-select-service" action={{ route('orders')}} method="GET">
            <div class="form-group">
              <label for="services">Dịch vụ</label>              
              <select class="form-control" id="services" onchange="this.form.submit()" name="service">
                <option value={{null}}>Tất cả</option>
                @foreach($services as $item)                  
                  <option value={{$item->id}} {{(isset($service) && $item->id==$service) ? 'selected="selected"' : null}}>{{$item->name}}</option>
                @endforeach              
              </select>
            </div>
          </form>            
        </div>               
        <div class="col col-md-6 col-sm-6">
            @component('components.search',['title' => 'Tìm kiếm', 'routerName' => 'orders', 'search' => (empty($search)?null:$search)])
            @endComponent
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <table id="menus" class="table table-bordered table-striped dataTable" role="grid">
            <thead>
              <tr role="row">
                <th style="width: 10%;">Mã</th>
                <th style="width: 10%;">Dịch vụ</th>
                <th style="width: 10%;text-align:center;">Tổng cộng</th>
                <th style="width: 10%;text-align:center;">Trạng thái</th>
                <th style="width: 30%;">Detect</th>
                <th style="width: 10%;">Loại hóa đơn</th>
                <th style="width: 50%;">Người tạo</th>                
                <th style="width: 50%;">Người sửa</th>
                <th style="width: 5%;text-align:center;">Chỉnh sửa</th>
                <th style="width: 5%;text-align:center">Xóa</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($data as $item)
                <tr role="row" class="odd">
                  <td>{{$item->code}}</td>
                  <td>
                    @if (empty($item->service))
                      <small class="label label-danger">Chưa phân loại</small>
                    @else
                      <small class="label label-success">{{(strlen ($item->service->name) > 50) ? (substr( $item->service->name,  0, 47) . '...') : $item->service->name}}</small>                      
                    @endif
                  </td>                  
                  <td style="text-align:center;">
                    @if (empty($item->total))
                      <small class="label label-danger">Không xác định</small>
                    @else
                      <span class="label label-warning">{{number_format($item->total) . ' VNĐ'}}</span>
                    @endif                    
                  </td> 
                  <td style="text-align:center;">
                    @if ($item->validator)
                      <small class="label label-success">Hợp lệ</small>
                    @else
                      <span class="label label-warning">Không hợp lệ</span>
                    @endif                    
                  </td>
                  <td>
                    {{(strlen ($item->text_detect) > 300) ? substr($item->text_detect, 2, 300) . '...' : $item->text_detect}}                    
                  </td>   
                  <td style="text-align:center;">
                    @if ($item->input)
                      <small class="label label-success">Đầu vào</small>
                    @else
                      <span class="label label-warning">Đầu ra</span>
                    @endif                    
                  </td>               
                  <td>{{$item->createdBy->name}}</td> 
                  <td>{{empty($item->updatedBy) ? '---' : $item->updatedBy->name}}</td>                                                 
                  <td align="center">
                    <a href="#" data-toggle="modal" data-target="{{ '#modal-detail-' . $item->id }}">
                      <i class="fa fa-edit"></i>
                    </a>
                    @include('order.detail', ['model' => $item, 'buttonName' => 'Cập nhật'])                   
                  </td>
                  <td align="center">
                    <a href="#" data-toggle="modal" data-target="{{ '#modal-delete-order-' . $item->id }}">
                      <i class="fa fa-trash-o"></i>
                    </a>
                    @include('order.delete', ['model' => $item])                    
                  </td>
                </tr>
              @endforeach
            </tbody>
            <tfoot>
              @if (count($data) > 10)
                <tr>
                    <th style="width: 10%;">Mã</th>
                    <th style="width: 10%;">Dịch vụ</th>
                    <th style="width: 10%;text-align:center;">Tổng cộng</th>
                    <th style="width: 10%;text-align:center;">Trạng thái</th>
                    <th style="width: 40%;">Detect</th>
                    <th style="width: 50%;">Người tạo</th>                
                    <th style="width: 50%;">Người sửa</th>
                    <th style="width: 5%;text-align:center;">Chỉnh sửa</th>
                    <th style="width: 5%;text-align:center">Xóa</th>
                </tr>
              @endif
            </tfoot>
          </table>
        </div>
      </div>
      @include('shared.paginate', ['pageShow' => 3, 'data' => $data])
    </div>
  </div>
  <!-- /.box-body -->
</div>
@endsection

@section('script')

  <script src="{{ asset('bower_components/admin-lte/dist/js/demo.js') }}"></script>     
  <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/uploadimage.js') }}"></script>
  <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>  
  <script src="{{ asset('bower_components/admin-lte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
  <!--datetime picker-->
  <script src="{{ asset('js/datetimePicker.js') }}"></script>
@endsection