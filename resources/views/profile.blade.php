@extends('layouts.app')

@section('title')
    <h1>Thông tin tài khoản</h1>  
@endsection

@section('content')
    <form class="form-horizontal" method="POST" action="{{ route('profile') }}">
        {{ csrf_field() }}
        @component('components.alert')       
          @if ($errors->any())
            <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
            </ul>
          @endif
        @endComponent
        <div class="form-group">
          <label for="name" class="col-sm-2 control-label">Tên</label>

          <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}">
          </div>
        </div>
        <div class="form-group">
            <label for="username" class="col-sm-2 control-label">Tài khoản</label>
  
            <div class="col-sm-10">
              <input type="text" class="form-control" id="username" disabled value="{{$user->username}}">
            </div>
        </div>
        <div class="form-group">
          <label for="email" class="col-sm-2 control-label">Email</label>

          <div class="col-sm-10">
            <input type="email" class="form-control" id="email" disabled value="{{$user->email}}">
          </div>
        </div> 
        <div class="form-group">                              
            <label for="role" class="col-sm-2 control-label">Quyền</label>  
            <div class="col-sm-10">
              <input type="text" class="form-control" id="role" disabled value="{{$user->role->name}}">
            </div>
        </div>                  
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-2 col-xs-12">
                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-fw fa-pencil-square-o"></i> Cập nhật chung</button>
            </div>    
            <div class="col-sm-offset-6 col-sm-2 col-xs-12">  
                <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal-auth-changepassword"><i class="fa fa-edit"></i> Thay đổi mật khẩu</a>                
            </div>                          
        </div>                
      </form>      
@endsection

@include('auth.passwords.change')
