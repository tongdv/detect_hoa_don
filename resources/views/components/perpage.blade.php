<form id="perpage-form" action="{{ route($routerName)}}" method="GET">
    {{ csrf_field() }}
    <div class="dataTables_length" id="perpage">
        <label>Hiển thị
            <select name="perpage" aria-controls="perpage" class="form-control input-sm" onchange="this.form.submit()">
                @foreach ($options as $option)
                    <option value="{{$option}}" {{($option==$default) ? 'selected="selected"' : null}}>{{$option}}</option>
                @endforeach
            </select> mục tin</label>
    </div>
</form>

