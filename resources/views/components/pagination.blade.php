<div class="row">
    <div class="col-sm-5">
      <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Hiển thị {{(($data->currentPage() -1) * $data->perPage()) + 1 }} tới {{$data->count()}} của {{$data->total()}} mục tin</div>
    </div>
    <div class="col-sm-7">
      <div class="dataTables_paginate paging_simple_numbers">
        <ul class="pagination">          
          <li class="paginate_button previous {{($data->currentPage() == 1) ? 'disabled' : null}}" >
            <a href="{{$data->appends(request()->query())->previousPageUrl()}}"><i class="fa fa-angle-left"></i></a>
          </li>        
          @if ($data->lastPage() > $pageShow)
            @if($data->currentPage()<4)
              @for ($i = 1; $i <= $pageShow; $i++)
                <li class="paginate_button {{($i == $data->currentPage()) ? 'active' : null}}">
                  <a href="{{$data->appends(request()->query())->url($i)}}">{{$i}}</a>              
                </li>
              @endfor  
            @else
             @for ($i = $data->currentPage()-2; $i <= $data->currentPage(); $i++)
                <li class="paginate_button {{($i == $data->currentPage()) ? 'active' : null}}">
                  <a href="{{$data->appends(request()->query())->url($i)}}">{{$i}}</a>              
                </li>
              @endfor  
            @endif
          @else
            @for ($i = 1; $i <= $data->lastPage(); $i++)
              <li class="paginate_button {{($i == $data->currentPage()) ? 'active' : null}}">
                <a href="{{$data->appends(request()->query())->url($i)}}">{{$i}}</a>
              </li>
            @endfor
          @endif                             
          <li class="paginate_button next {{($data->lastPage() == $data->currentPage()) ? 'disabled' : null}}">
            <a href="{{$data->appends(request()->query())->nextPageUrl()}}"><i class="fa fa-angle-right"></i></a>
          </li>          
        </ul>
      </div>
    </div>
  </div>