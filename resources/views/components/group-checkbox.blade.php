<label for={{$id}} class="control-label">{{isset($title) ? $title : null}}</label>
<div id={{$id}} style="margin-top: 5px;">
    <input type="radio" value={{$value_active}} name={{$name}}  {{($value == $value_active) ? 'checked' : null}} {{ (isset($disabled) && ($disabled == true)) ? 'disabled' : null }}> {{$title_active}} &nbsp;&nbsp;
    <input type="radio" value={{$value_inactive}} name={{$name}} {{($value == $value_inactive) ? 'checked' : null}} {{(isset($disabled) && ($disabled == true)) ? 'disabled' : null}} > {{$title_inactive}}
</div>