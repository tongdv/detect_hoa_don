<form id="search-form" action="{{ route($routerName)}}" method="GET">
    {{ csrf_field() }}
    <label>{{$title}}:
        <input type="search" class="form-control input-sm" value="{{$search}}" name="search">
    </label>  
</form>
    
    