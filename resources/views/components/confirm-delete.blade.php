@component('components.modals', [ 
    'idModal' => 'modal-' . ($type . '-' . $data->id), 
    'title' => (empty($title) ? __('system.comfirm_delete') : $title),    
    'width' => (empty($width) ? '30%' : $width)])
<form class="form-horizontal" action="{{ route($route, $data->id) }}" method="post" onsubmit="document.getElementById('submit').disabled=true">
    {{ csrf_field() }}
    {{ method_field('delete') }}
    <div class="modal-body">                                        
        {{$slot}}     
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left btn-flat" data-dismiss="modal">
            <i class="fa fa-close"></i> {{__('button.close')}}
        </button>
        <button type="submit" class="btn btn-flat bg-navy">
            <i class="fa fa-check"></i> {{isset($buttonName) ? $buttonName : __('button.delete')}}
        </button>
    </div>
</form>
@endcomponent