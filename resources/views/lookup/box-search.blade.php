@component('components.box-search',['routerName' => 'lookup', 'search' => (empty($search)?null:$search)])
    <div class="form-group1">
        <label for="filter">Tìm kiếm theo loại</label>
        @component('components.select',['data'=>$loaiLookups,
        'text' => 'ten',
        'value' => 'ma',
        'name' => 'search_loai',
        'idSelected'=>$search_loai,
        'tatca'=>'Tất cả',
        'none_required'=>'true'])
        @endcomponent
    </div>
    <br/>
    <div class="form-group1">
        <label for="contain">Tìm kiếm theo trạng thái</label>
        @component('components.select',['data'=>$actives,
            'text' => 'ten',
            'value' => 'ma',
            'name' => 'search_trang_thai',
            'idSelected'=>$search_trang_thai,
            'tatca'=>'Tất cả',
            'none_required'=>'true'])
        @endcomponent
    </div>
@endComponent