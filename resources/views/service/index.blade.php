@extends('layouts.app') 

@section('css')
  <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">  
  <link rel="stylesheet" href="{{ asset('css/chip.css') }}">  
@endsection

@section('title')
  <h1>Danh mục dịch vụ</h1>
@endsection 

@section('content')
<div class="box">
  <div class="box-header">
      @component('components.alert')               
      @endComponent   
    <div class="row">     
      <div class="col-xs-12 pull-right">
        <a href="#" data-toggle="modal" data-target="{{ '#modal-add-service'}}" class="btn bg-olive flat">
          <i class="fa fa-plus"> Thêm mới</i>
        </a>                   
        @include('service.add')       
      </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-6">
          @component('components.perpage',['options' => [10,20,50,100], 'default'=> $data->perPage(),'data' => $data, 'routerName' => 'services'])
          @endComponent
        </div>
        <div class="col-sm-6">          
          <div id="search" class="dataTables_filter">
            @component('components.search',['title' => 'Tìm kiếm', 'routerName' => 'services', 'search' => (empty($search)?null:$search)])
            @endComponent           
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <table id="services" class="table table-bordered table-striped dataTable" role="grid">
            <thead>
              <tr role="row">
                <th style="width: 20%;">Phân loại</th> 
                <th style="width: 20%;">Tên</th>
                <th style="width: 25%;">Mô tả</th>                                               
                <th style="width: 20%;">Từ khóa</th>
                <th style="width: 5%;">Trạng thái</th>                
                <th style="width: 5%;text-align:center;">Chỉnh sửa</th>
                <th style="width: 5%;text-align:center">Xóa</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($data as $service)
                <tr role="row">
                  <td>{{isset($service->categoryLookup) ? $service->categoryLookup->ten : '---'}}</td>    
                  <td>{{$service->name}}</td>
                  <td>{{$service->description}}</td>                                                  
                  <td>
                    @if(isset($service->tu_khoas))
                      @foreach($service->tu_khoas as $item)
                        <div class="chip">{{$item->tu_khoa}}</div>
                      @endforeach
                    @endif                   
                  </td>                                  
                  <td style="text-align:center;">
                    @if ($service->active)
                      <small class="label bg-olive flat block">Đang dùng</small>
                    @else
                      <small class="label bg-navy flat block">Ngừng sử dụng</small>
                    @endif
                  </td>
                  <td align="center">
                    <a href="#" data-toggle="modal" data-target="{{ '#modal-update-service-' . $service->id }}">
                      <i class="fa fa-edit"></i>
                    </a> 
                    @include('service.detail', ['model' => $service, 'buttonName' => 'Cập nhật'])                                    
                  </td>
                  <td align="center">
                    <a href="#" data-toggle="modal" data-target="{{ '#modal-delete-service-' . $service->id }}">
                      <i class="fa fa-trash-o"></i>
                    </a>  
                    @include('service.delete', ['model' =>  $service])                     
                  </td>
                </tr>                                                 
              @endforeach
            </tbody>
            <tfoot>
              @if (count($data) > 10)
                <tr>
                  <th style="width: 20%;">Phân loại</th> 
                  <th style="width: 20%;">Tên</th>
                  <th style="width: 25%;">Mô tả</th>                                               
                  <th style="width: 20%;">Từ khóa</th>
                  <th style="width: 5%;">Trạng thái</th>                
                  <th style="width: 5%;text-align:center;">Chỉnh sửa</th>
                  <th style="width: 5%;text-align:center">Xóa</th>
                </tr>
              @endif
            </tfoot>
          </table>
        </div>
      </div>
      @include('shared.paginate', ['pageShow' => 3, 'data' => $data])
    </div>
  </div>
  <!-- /.box-body -->
</div>
@endsection

@section('script')
  <script src="{{ asset('bower_components/admin-lte/dist/js/demo.js') }}"></script>     
  <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script> 
  <script src="{{ asset('js/orderadd.js') }}"></script> 
@endsection