@component('components.modal-add', [ 
    'type' => 'service', 
    'title' => 'Thêm mới dịch vụ',
    'width' => '45%',
    'route' => 'services.add',
    ])
    <div class="row">
        <div class="col-sm-12">            
            <label for="inputName" class="control-label">Tên</label>
            <input type="text" class="form-control" id="inputName" name="name" tabindex="1" autofocus>

            <label for="inputDescription" class="control-label">Mô tả</label>
            <textarea class="form-control" id="inputDescription" placeholder="mô tả ..." name="description" tabindex="2"></textarea>

            <label for="inputRatio" class="control-label">Tỷ lệ</label>
            <input type="text" class="form-control" id="inputRatio" name="ratio" tabindex="3">

            <label for="category" class="control-label">Loại dịch vụ</label>        
            @component('components.select', ['data' => $categories, 
                'text' => 'ten', 
                'name' => 'category', 
                'value' => 'ma',
                'id' => 'category', 
                'idSelected'=>  old('category'),
                'tabindex'=> 4
            ])
            @endcomponent

            @component('components.group-checkbox', [
                'title' => 'Còn sử dụng',
                'id' => 'active',
                'name' => 'active',
                'title_active' => 'Có',
                'title_inactive' => 'Không',
                'value_active' => 1,
                'value_inactive' => 0,
                'value' => old('active',1),                         
            ])
            @endcomponent                        
        </div>        
    </div> 
    <br/>
    <div class="input-group">
        <input type="text" class="form-control" id="input-keyword">
        <span class="input-group-btn">
            <button type="button" class="btn bg-olive btn-flat" onclick="themtukhoa()" > Thêm từ khóa</button>
        </span>
    </div>
    <br/>
    <div class="row">        
        <div class="col-md-12" id="keywords">                
        </div>
    </div>
@endcomponent