@component('components.modal-update', [ 
    'type' => 'update-service',
    'title' => 'Cập nhật thông tin dịch vụ',
    'width' => '45%',
    'route' => 'services.update',
    'data' => $model,
    'method' => 'put'])

    <div class="row">
        <div class="col-md-12">
            <label class="control-label">Name</label>
            <input type="text" class="form-control" value="{{$model->name}}" name="name" tabindex="1" autofocus>

            <label class="control-label">Mô tả</label>
            <textarea class="form-control" placeholder="mô tả ..." name="description" tabindex="2">{{$model->description}}</textarea> 
            
            <label class="control-label">Tỷ lệ</label>
            <input type="text" class="form-control" name="ratio" tabindex="5" value="{{$model->ratio}}">

            <label for="category" class="control-label">Loại dịch vụ</label>        
            @component('components.select', ['data' => $categories, 
                'text' => 'ten', 
                'name' => 'category', 
                'value' => 'ma',
                'id' => 'category', 
                'idSelected'=>  $model->category,
                'tabindex'=> 4
            ])
            @endcomponent

            @component('components.group-checkbox', [
                'title' => 'Còn sử dụng',
                'id' => 'active',
                'name' => 'active',
                'title_active' => 'Có',
                'title_inactive' => 'Không',
                'value_active' => 1,
                'value_inactive' => 0,
                'value' => $model->active                        
            ])
            @endcomponent                        
        </div>        
    </div>      
    <br>  
    <div class="row">
        <div class="col-md-12">
            <div class="input-group block">
                <input type="text" class="form-control" id={{'input-keyword-detail-' . $model->id}}>
                <span class="input-group-btn">
                    <button type="button" class="btn bg-olive btn-flat" onclick={{'themtukhoa(' . $model->id . ')'}}> Thêm từ khóa</button>
                </span>
            </div>
        </div>
    </div>      
    <br>    
    <div class="row">        
        <div class="col-md-12" id={{'keywords-detail-' . $model->id}}>
            @foreach($model->tu_khoas as $item)
                <div class="chip">
                    {{$item->tu_khoa}}<i class="close fa fa-times" onclick="removeKeyWord(this)"></i>
                    <input type="hidden" name="key_words[]" value="{{$item->tu_khoa}}"></input>
                </div>
            @endforeach              
        </div>
    </div>
@endcomponent