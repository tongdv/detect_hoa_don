@component('components.confirm-delete',
[
    'route' => 'services.delete',
    'method' => 'delete',
    'data' => $model,
    'type' => 'delete-service',
    'title' => 'Xóa dịch vụ'
])

<div class="row">
    <div class="col-sm-12">
        <label class="control-label">Tên</label>
        <input type="text" class="form-control" value="{{$model->name}}" name="code" disabled>
    
        <label class="control-label">Mô tả</label>           
        <input type="text" class="form-control" value="{{$model->name}}" name="name" disabled>                    
    </div>
</div>
@endcomponent