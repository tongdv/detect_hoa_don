<?php

return [

    /*
    |--------------------------------------------------------------------------
    | System module Language Lines
    |--------------------------------------------------------------------------
    |   
    |
    */

    'create_menu_success' => 'Thêm mới menu thành công.',
    'create_role_success' => 'Thêm mới quyền thành công.',
    'create_success' => 'Thêm mới thành công.',
    'create_menu_error' => 'Lỗi dữ liệu thêm mới menu.',
    'create_role_error' => 'Lỗi dữ liệu thêm mới quyền.',
    'delete_success'  => 'Xóa thành công.',
    'update_success' => 'Cập nhật thông tin thành công.',
    'create_menu_success' => 'Thêm mới menu thành công.',
    'create_role_success' => 'Thêm mới quyền thành công.',
    'create_success' => 'Thêm mới thành công.',
    'create_menu_error' => 'Lỗi dữ liệu thêm mới menu.',
    'create_role_error' => 'Lỗi dữ liệu thêm mới quyền.',
    'create_error' => 'Lỗi thêm mới dữ liệu',
    'delete_error' => 'Lỗi xóa dự liệu',
    'delete_success'  => 'Xóa thành công.',
    'delete_menu_success' => 'Xóa menu thành công',
    'update_success' => 'Cập nhật thông tin thành công.',
    'update_error'=>'Lỗi cập nhật dữ liệu.',
    'update_menu_error' => 'Lỗi dữ liệu cập nhật menu',
    'update_role_error' => 'Lỗi dữ liệu cập nhật quyền',
    'add_lookup' => 'Tạo danh mục dùng chung',
    'validator' => 'Lỗi dữ liệu',

];
