/**
 * Download xml theo mau khai bao thue
 */
function downloadXml() {    
    $param = new Array();    
    $fileName = 'ky_khai';
    if($('#search').length > 0 && $('#search').val() != '') {
        $param['search'] = $('#search').val();        
    }

    if($('#search_month').length > 0 && $('#search_month').val() != '') {
        $param['search_month'] = $('#search_month').val();
        $fileName = $fileName + '_' + $param['search_month'];
    }    

    if($('#search_dich_vu').length > 0 && $('#search_dich_vu').val() != '') {
        $param['search_dich_vu'] = $('#search_dich_vu').val();
    }
    
    return download('report/sumtotal', null, $param, $fileName);
}