function themtukhoa(modelId){
    var idElementInput = 'input-keyword';
    var idElementKeyWord = 'keywords';
    
    if(modelId) {
        idElementInput = 'input-keyword-detail-' + modelId;
        idElementKeyWord = 'keywords-detail-' + modelId;
    }

    var elementInput = document.getElementById(idElementInput);
    var elementKeywords = document.getElementById(idElementKeyWord);

    if((elementInput) && (elementKeywords) && (elementInput.value != '')) {
        var keywordChipElement = document.createElement("div");
        keywordChipElement.setAttribute('class', 'chip');
        keywordChipElement.innerHTML = elementInput.value + '<i class="close fa fa-times" onclick="removeKeyWord(this)"></i>';               
        var keywordElementHiden = document.createElement("input");
        keywordElementHiden.setAttribute('type', 'hidden');
        keywordElementHiden.setAttribute('name', 'key_words[]');
        keywordElementHiden.setAttribute('value', elementInput.value);        
        elementKeywords.appendChild(keywordChipElement);
        elementKeywords.appendChild(keywordElementHiden);
        elementInput.value = null;
    }    
}

function removeKeyWord(row) {
    row.parentNode.remove();    
}

