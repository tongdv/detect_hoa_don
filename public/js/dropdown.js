function open_dropdown(dropdown_id) {
    var dropdown_menu = document.getElementById(dropdown_id);
    if (dropdown_menu) {
        if (dropdown_menu.classList.contains('open')) {
            dropdown_menu.classList.remove("open");
        }
        else {
            dropdown_menu.classList.add("open");
        }
    }
}