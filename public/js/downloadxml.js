function download(route, buttonId = 'btnXuatXml', param = Array, fileName = 'file.xml') {
    if (route != undefined) {
        var btnXuatXml = document.getElementById(buttonId);
        if (btnXuatXml) {
            btnXuatXml.disabled = true;
        }

        var ajax = new XMLHttpRequest();
        ajax.responseType = 'blob';

        ajax.addEventListener("load", function (e) {
            if (btnXuatXml) {
                btnXuatXml.disabled = false;
            }
            if (e.target.status == 200) {
                var disposition = ajax.getResponseHeader('content-disposition');
                var matches = /"([^"]*)"/.exec(disposition);
                var filename = (matches != null && matches[1] ? matches[1] : fileName);

                var blob = new Blob([ajax.response], { type: 'application/xml' });
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = filename;

                document.body.appendChild(link);

                link.click();

                document.body.removeChild(link);
            }
        }, false);

        ajax.addEventListener("error", function (e) {
            if (btnXuatXml) {
                btnXuatXml.disabled = true;
            }
        }, false);

        ajax.addEventListener("abort", function (e) {
            if (btnXuatXml) {
                btnXuatXml.disabled = true;
            }
        }, false);

        var strUrl = '?';
        param.forEach(element => {

        });
        for (var key in param) {
            strUrl = strUrl + key + '=' + param[key] + '&';
        }

        ajax.open("get", '/'+ route + strUrl);
        ajax.setRequestHeader('Accept', 'application/xml');
        ajax.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr("content"));

        var uploaderForm = new FormData();
        ajax.send(uploaderForm);
    }
}