<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'avatar_url', 'active', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getAvatarUrlAttribute()
    {
        if(empty($this->attributes['avatar_url'])){
            return "images/defaults/avatar.png";
        }

        return $this->attributes['avatar_url'];
    }

    public function role(){
        return $this->belongsTo('App\Role');
    }

    protected $dates = [
        'created_at',
        'updated_at',        
    ];
}
