<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KeyWord extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tu_khoa', 'service_id', 'words'
    ]; 


    /**
     * Get the service record associated with the order
     */
    public function service()
    {
        return $this->belongsTo('App\Service');
    }
    
    public $timestamps = false;
}
