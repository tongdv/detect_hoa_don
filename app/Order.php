<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text_detect', 'service_id', 'created_by', 'updated_by', 'url_image', 'code', 'total', 'validator', 'input'
    ];

    protected $dates = [
        'created_at',        
        'updated_at',        
    ];

    /**
     * Get the service record associated with the order
     */
    public function service()
    {
        return $this->belongsTo('App\Service');
    }

    public function createdBy(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function updatedBy(){
        return $this->belongsTo('App\User', 'updated_by', 'id');
    }
}
