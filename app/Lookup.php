<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lookup extends Model
{        
    protected $table='lookup';

    public $timestamps=false;

    protected $fillable = [
        'ma', 'ten', 'loai', 'active'
    ];

    public function type_lookup() {
        return $this->belongsTo('App\Lookup', 'loai', 'ma');
    }
}
