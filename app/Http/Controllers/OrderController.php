<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Google\Cloud\Vision\VisionClient;
use App\Menu;
use App\Order;
use App\Role;
use App\KeyWord;
use App\Service;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App\StringHelper;
use Image;
use App\Lookup;

class OrderController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        $user = Auth::user();
        
        $menus = Menu::query()            
                ->ofRole($user->role_id)
                ->with(['children' => function($query) use($user) {
                    $query->ofRole($user->role_id)
                        ->orderBy('order');
                }, 'roles'])
                ->firstLevel()                        
                ->orderBy('order')
                ->get();            

        $perPage = $request->get('perpage', 15);
        $page = $request->get('page', 1);
        $search = $request->get('search');
        $service = $request->get('service');
        $datefilter = $request->get('datefilter');        
        
        $query = Order::query();

        if(isset($search)) {
            $query->where(function($query) use($search){
                $query->orWhere('text_detect', 'like', "%{$search}%");
                $query->orWhere('code', 'like', "%{$search}%");                
            });
        }

        if(isset($service)) {
            $query->whereHas('service', function ($query) use($service) {
                $query->where('id', '=', $service);
            });
        }        

        if(isset($datefilter)) {            
            $startTime = Carbon::createFromFormat('d/m/Y', substr($datefilter, 0, 10));
            $endTime = Carbon::createFromFormat('d/m/Y', substr($datefilter, 13, 10));            
        }
        else{
            $startTime = Carbon::now()->addDays(-7);
            $endTime = Carbon::now()->addDays(7); 
            $datefilter = Carbon::now()->addDays(-7)->format('d/m/Y') . ' - ' . Carbon::now()->addDays(7)->format('d/m/Y');           
        }
        $query->whereBetween('updated_at', array($startTime->startOfDay()->toDateTimeString(), $endTime->endOfDay()->toDateTimeString()));

        $query->with(['service']);

        $query->orderBy('updated_at', 'desc');

        $data = $query->paginate($perPage, ['*'], 'page', $page);        
        
        return view('order.index', [
            'menus' => $menus, 
            'data' => $data, 
            'search'=> $search,
            'services' => Service::query()->where('active', true)->get(),
            'service' => $service, 
            'datefilter' => $datefilter,                          
        ]);
    }

    public function addByDetect(Request $request) {
        if($request->hasFile('imageOrder')) {            
            $imageOrder = $request->imageOrder;                                

            $fileName = time() . '-' . $imageOrder->getClientOriginalName();

            $imageOrder->storeAs(
                'public/orders', $fileName
            );
           
            $path = "storage/orders/" . $fileName;                        

            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);  
            
            $x1 = $request->get('x1');
            $y1 = $request->get('y1');
            $x2 = $request->get('x2');
            $y2 = $request->get('y2');           

            if(!empty($x1) && !empty($y1) && !empty($x2) && !empty($y2)) {                                         
                $image = Image::make($path);                                         
                $width = $image->width();
                $height = $image->height();
               
                $height_new = $y2 - $y1;
                $width_new = $x2 - $x1;
                if($width_new > $width) {
                    $width_new = $width;
                }
                if($height_new > $height) {
                    $height_new = $height;
                }                

                $image->resize(500, 500);
                $image->crop($width_new, $height_new, $x1, $y1)->save();
            }            
            if(isset($image))
                $base64 = base64_encode($image);
            else
                $base64 = base64_encode($data);

            $textDetectResult = $this->detect_order($base64, config('application.goolgevison', 'AIzaSyBkjSpBgI9pvtzJf7qJoW3rNCmVmwj2fFA'));            
            
            $user = Auth::user();
            $infoOrder = array();
            $infoOrder['url_image'] = $path;
            $infoOrder['created_by'] = $user->id;
            $infoOrder['code'] = time();
            $infoOrder['input'] = $request->get('input');
            $totalOrderDetect = 0;

            if(!empty($textDetectResult)) {                               
                $infoOrder['text_detect'] = json_encode($textDetectResult['words'], true);                

                $services = Service::query()
                    ->with(['tu_khoas'])
                    ->active()
                    ->get();                    

                $keyWordsDetectTotalOrder = Lookup::query()
                    ->where('loai', 'key_word_detect_total_order')
                    ->where('active', true)
                    ->pluck('ma')
                    ->toArray();
                
                foreach($keyWordsDetectTotalOrder as $key => $value) {
                    $keyWordsDetectTotalOrder[$key] = StringHelper::vn_to_str($value);                    
                }                                
                
                foreach ($textDetectResult['words'] as $key => $value) {                        
                    $textDetectResult['words'][$key] = StringHelper::vn_to_str($value);
                }                

                foreach($services as $service) {
                    $service->rate = 0;                                        
                    $words = array();
                    foreach($service->tu_khoas as $keyword) {
                        $words = array_merge($words, json_decode($keyword->words, true));
                    }
                    
                    foreach ($textDetectResult['words'] as $value) {                        
                        if (in_array($value, $words)) {
                            $service->rate ++;                            
                        }
                    }                    
                }                            
                
                if(array_key_exists('lines', $textDetectResult)) { 
                    foreach ($textDetectResult['lines'] as $key => $value) {                         
                        foreach($keyWordsDetectTotalOrder as $keyWord) {                             
                            if(strcmp($keyWord, $value) == 0) { 
                                $numberLineNext = 12;
                                for($j =0 ;  $j < $numberLineNext; $j++) {
                                    if(isset($textDetectResult['lines'][$key + $j])) {
                                        $nextLine = $textDetectResult['lines'][$key + $j];                         
                                        if(!empty($nextLine)) {
                                            $totalOrder = preg_replace('/\s+/', '', $nextLine);
                                            $totalOrder = str_replace('.', '', $totalOrder);                                            
                                            $totalOrder = str_replace(',', '', $totalOrder);                                            
                                            if(is_numeric($totalOrder)) {
                                                $totalOrderDetect = (double) $totalOrder;
                                                break;
                                            }
                                        }
                                    }                                                                        
                                }  
                                if($totalOrderDetect > 0) {
                                    break;
                                }                              
                            }  
                        }                                                
                    }                                            
                }
                
                if($totalOrderDetect > 0) {
                    $infoOrder['total'] = $totalOrderDetect;
                    if($infoOrder['total'] < env('TOTAL_ORDER_LIMIT', 20000000)) {
                        $infoOrder['validator'] = true;
                    }
                }

                if(count($services) > 0) {
                    $selectedService = $services[0];

                    foreach($services as $service) {
                        if($service->rate > $selectedService->rate) {
                            $selectedService = $service;
                        }
                    }

                    if($selectedService->rate > 0) {
                        $infoOrder['service_id'] = $selectedService->id;

                        Order::query()->create($infoOrder);  
                                                  
                        return back()->withInput()
                            ->with('alert-type', 'alert-success')
                            ->with('alert-content', 'Nhận diện hóa đơn thành công.');
                    }                    
                }                               
            }
            
            Order::query()->create($infoOrder);

            return back()->withInput()                
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', 'Hóa đơn không thể nhận diện dịch vụ tương ứng. Xem chi tiết trong danh sách.');
        }   
        
        return back()->withInput()                
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', 'Lỗi thiếu file ảnh hóa đơn.');
    }    

    public function detect_order($base64, $key)
    {
        $result = array();
        $words = array();
        $textLine = array();

        $googleVisionApiUrl = 'https://vision.googleapis.com/v1/images:annotate?key=' . $key;

        $type = "TEXT_DETECTION";

        $params = '{
            "requests": [
              {
                "image": {
                  "content":"' . $base64. '"
                },
                "features": [
                    {
                        "type": "TEXT_DETECTION",
                        "maxResults": 200
                    }
                ]
              }
          ]
        }';                                       
        
        $client = new Client;

        $response = $client->post($googleVisionApiUrl, [
            'headers' => [
                'Content-type' => 'application/json',
            ],
            'body' => $params
        ]);

        $data = json_decode($response->getBody(), true);		
        
        if(isset($data['responses'])) {
            $response = $data['responses'];
            if(isset($response[0])) {
                $resultDetect = $response[0];
                if(isset($resultDetect['textAnnotations'])) {                    
                    $textAnnotations = $resultDetect['textAnnotations'];
                    if(is_array($textAnnotations)) {
                        foreach($textAnnotations as $item) {
                            if (array_key_exists('description', $item)) {
                                $textInline = trim(preg_replace('/\s+/', ' ', $item['description']));
                                // loai bo chuoi full
                                if (!preg_match('/\s/',$textInline) ) {
                                    $words[] = $textInline;
                                }                                
                            }
                        }
                    }                                                                        
                }

                if(isset($resultDetect['fullTextAnnotation'])) {                    
                    $fullTextAnnotations = $resultDetect['fullTextAnnotation'];
                    if(array_key_exists('text', $fullTextAnnotations)) {
                        $textLine = explode("\n", $fullTextAnnotations['text']);
                        foreach($textLine as $key => $line) {
                            if(empty($line)) {
                                unset($textLine[$key]);
                            }  
                            $textLine[$key] = str_replace(':', '', StringHelper::vn_to_str($line));                        
                        }                       
                    }                                                                                           
                }
            }
        }  
        
        $result['words'] = $words;
        $result['lines'] = $textLine;
        
        return $result;
    }  
    
    public function delete($id) {
        
        Order::destroy($id);

        return back()->withInput()
                ->with('alert-type', 'alert-success')
                ->with('alert-content', 'Xóa dữ liệu thành công.');
    }

    public function update(Request $request, $id) {
        $order = Order::query()->findOrFail($id);
        $user = Auth::user();
        $info = $request->only([ 'service_id', 'text_detect', 'total', 'input']);                            

        $info['updated_by'] = $user->id;

        if(isset($info['total']) && $info['total'] > 0) { 
            $info['total'] = preg_replace('/\s+/', '', $info['total']);
            $info['total'] = str_replace('.', '', $info['total']);                                            
            $info['total'] = str_replace(',', '', $info['total']); 
            
            if($info['total'] < env('TOTAL_ORDER_LIMIT', 20000000)) {
                $info['validator'] = true;
            }
            else{
                $info['validator'] = false;
            }
        }
        
        $order->update($info);

        return back()->withInput()
                ->with('alert-type', 'alert-success')
                ->with('alert-content', 'Cập nhật hóa đơn thành công.');
    }

    public function showFormAdd() {
        $user = Auth::user();
        
        $menus = Menu::query()            
                ->ofRole($user->role_id)
                ->with(['children' => function($query) use($user) {
                    $query->ofRole($user->role_id)
                        ->orderBy('order');
                }, 'roles'])
                ->firstLevel()                        
                ->orderBy('order')
                ->get(); 
        
        return view('order.add', [
            'menus' => $menus,                                     
        ]);
    }
}
