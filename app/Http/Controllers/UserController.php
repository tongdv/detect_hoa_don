<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Menu;
use App\User;
use App\Role;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);        
    }

    public function index(Request $request){
        
        $user = Auth::user();

        $menus = Menu::query()            
                ->ofRole($user->role_id)
                ->with(['children' => function($query) use($user) {
                    $query->ofRole($user->role_id)
                        ->orderBy('order');
                }, 'roles'])
                ->firstLevel()                        
                ->orderBy('order')
                ->get();
        
        $roles = Role::query()->get();
        
        $perPage = $request->get('perpage', 10);
        $page = $request->get('page', 1);
        $search = $request->get('search');
        
        $query = User::query();

        if(isset($search)) {
            $query->where(function($query) use($search){
                $query->orWhere('name', 'ilike', "%{$search}%");
                $query->orWhere('username', 'ilike', "%{$search}%");
                $query->orWhere('email', 'ilike', "%{$search}%");
            });
        }
        $query->orderBy('updated_at','desc');
        $users = $query->paginate($perPage, ['*'], 'page', $page);        
        
        return view('system.user.index', [
            'menus' => $menus, 
            'data' => $users, 
            'search'=> $search,
            'roles' => $roles,                        
        ]);
    }

    public function show($id){        
        return view('system.user-detail', 
            ['user' => User::findOrFail($id)]
        );
    }

    public function update(Request $request, $id){        
        $user = User::query()->findOrFail($id);
        
        $info = $request->only(['name', 'email', 'role_id', 'password', 'active']);
        $validator = Validator::make($info, [           
            'name' => 'max:255',
            'email' => 'email|max:255',
            'role_id' => 'exists:roles,id',
            'password' => 'max:255',
            'active' => 'boolean'
        ]);

        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', __('user.update_user_error'));
        }

        if(!empty($info['password'])) {
            $info['password'] = Hash::make($request->password);
        }
        else{
            unset($info['password']);
        }        
        
        if(empty($info['active'])) {
            $info['active'] = false;
        }

        $user->update($info);

        return back()->withInput()
                ->with('alert-type', 'alert-success')
                ->with('alert-content', __('system.update_success'));
    }

    public function delete($id) {                
        $user = Auth::user();

        if($user->id == $id){   
            return back()->withInput()
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', __('user.delete_user_error_yourself'));
        }
        else{
            User::find($id)->update(['active' => false]);

            return back()->withInput()
                ->with('alert-type', 'alert-success')
                ->with('alert-content', __('user.delete_user_success'));
        }                        
    }

    /**
     * Thêm mới người dùng
     */
    public function add(Request $request) {
        
        $info = $request->all(); 
        
        $validator = Validator::make($info, [
            'username' => 'required|unique:users|max:255',
            'name' => 'required|max:255',
            'email' => 'email|max:255',
            'role_id' => 'required|exists:roles,id',
            'password' => 'required|max:255|min:6|confirmed', 
            'active' => 'boolean'           
        ]);

        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', __('user.create_user_error'));
        }            
        
        if(isset($info['password'])) {
            $info['password'] = Hash::make($info['password']);
        }

        if(empty($info['active'])) {
            $info['active'] = true;
        }
        
        $user = User::query()
            ->create($info);
        
        return back()->withInput()
                ->with('alert-type', 'alert-success')
                ->with('alert-content', __('user.create_user_success'));
    }
}
