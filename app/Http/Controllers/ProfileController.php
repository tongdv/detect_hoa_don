<?php

namespace App\Http\Controllers;

use App\Menu;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use Exception;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showProfileForm()
    {
        $user = Auth::user();
        
        $menus = Menu::query()            
            ->ofRole($user->role_id)
            ->with(['children' => function($query) use($user) {
                $query->ofRole($user->role_id)
                    ->orderBy('order');
            }, 'roles'])
            ->firstLevel()                        
            ->orderBy('order')
            ->get(); 
                      
        $user = Auth::user();

        return view('profile', ['menus' => $menus, 'user' => $user]);
    }

    /**
     * Handle a update profile request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {              
        $info = $request->only('name');       
        
        $validator = Validator::make($info, [            
            'name' => 'required|max:255',                  
        ]);

        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', 'Lỗi dữ liệu cập nhật user.');                               
        }            
        
        $user = User::query()
            ->update($info);        
        
        return back()->withInput()
                ->with('alert-type', 'alert-success')
                ->with('alert-content', 'Cập nhật người dùng thành công.'); 
    }

    public function changePassword(Request $request){
        $info = $request->all();

        $validator = Validator::make($info, [            
            'password' => 'required',            
            'new_password' => 'required|max:255|min:6|confirmed'                    
        ]);

        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', 'Lỗi dữ liệu cập nhật mật khẩu.');                               
        }

        $user = Auth::user();        
       
        if(!Hash::check($info['password'], $user->password)){            
            return back()->withInput()
                ->withErrors($validator)
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', 'Mật khẩu hiện tại không đúng.');  
        }

        User::query()
            ->find($user->id)
            ->update(['password' => Hash::make($info['new_password'])]);

        return back()->withInput()
            ->with('alert-type', 'alert-success')
            ->with('alert-content', 'Cập nhật mật khẩu thành công.'); 
    }
}
