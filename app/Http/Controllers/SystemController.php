<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Role;
use App\Menu;
use App\RoleMenu;
use DB;
use Illuminate\Validation\Rule;


class SystemController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Get listing menu
     * 
     * thangnv create
     */
    public function indexMenu(Request $request){
        $user = Auth::user();

        $menus = Menu::query()            
                ->ofRole($user->role_id)
                ->with(['children' => function($query) use($user) {
                    $query->ofRole($user->role_id)
                        ->orderBy('order');
                }, 'roles'])
                ->firstLevel()                        
                ->orderBy('order')
                ->get();  

        $perPage = $request->get('perpage', 10);
        $page = $request->get('page', 1);
        $search = $request->get('search');
        
        $query = Menu::query();

        if(isset($search)) {
            $query->where(function($query) use($search){                
                $query->orWhere('name', 'ilike', "%{$search}%");
                $query->orWhere('router_link', 'ilike', "%{$search}%");
                $query->orWhere('fa_icon', 'ilike', "%{$search}%");                
            });  
        }

        $query->orderBy('parent_id', 'desc');

        $data = $query->paginate($perPage, ['*'], 'page', $page);        
        
        return view('system.menus.index', [
            'menus' => $menus, 
            'data' => $data, 
            'search'=> $search,
            'menu_parents' => Menu::query()->get()                                   
        ]);
    }
 

    public function addMenu(Request $request){
        $info = $request->all(); 
        
        $validator = Validator::make($info, [
            'name' => 'required|max:255',
            'router_link' => 'required|max:255',
            'fa_icon' => 'required',
            'order' => 'required|numeric|min:1'                        
        ]);

        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', __('system.create_menu_error'));
        }            
        
        $menu = Menu::query()
            ->create($info);
        
        return back()->withInput()
                ->with('alert-type', 'alert-success')
                ->with('alert-content', __('system.create_menu_success'));
    
    }

    public function updateMenu(Request $request, $id){
       
        $menu = Menu::query()->findOrFail($id);
        
        $info = $request->only([ 'parent_id','name','router_link', 'fa_icon','order', 'active']);
        $validator = Validator::make($info, [
            'name' => 'required|max:255',
            'router_link' => 'required|max:255',
            'fa_icon' => 'required',
            'order' => 'required|numeric|min:1'
        ]);

        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', __('system.update_menu_error'));
        }

        if(empty($info['active'])) {
            $info['active'] = false; 
        }

        $menu->update($info);

        return back()->withInput()
                ->with('alert-type', 'alert-success')
                ->with('alert-content', __('system.update_success'));
    }

    public function deleteMenu(Request $request, $id){
        try{
            DB::beginTransaction();

            RoleMenu::query()
                ->where('menu_id', $id)
                ->delete();
        
            Menu::destroy($id);

            DB::commit();
    
            return back()->withInput()
                ->with('alert-type', 'alert-success')
                ->with('alert-content',__('system.delete_menu_success'));
        }

        catch(Exception $exception){
            DB::rollBack();

            return back()                
                ->withInput()
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', __('system.delete_menu_error'));
        }  
    }        
    
    public function indexRole(Request $request) {
        $user = Auth::user();

        $menus = Menu::query()            
                ->ofRole($user->role_id)
                ->with(['children' => function($query) use($user) {
                    $query->ofRole($user->role_id)
                        ->orderBy('order');
                }, 'roles'])
                ->firstLevel()                        
                ->orderBy('order')
                ->get();          

        $perPage = $request->get('perpage', 10);
        $page = $request->get('page', 1);
        $search = $request->get('search');
        
        $query = Role::query();

        if(isset($search)) {
            $query->where(function($query) use($search){
                $query->orWhere('name', 'ilike', "%{$search}%");
                $query->orWhere('code', 'ilike', "%{$search}%");
                $query->orWhere('description', 'ilike', "%{$search}%");
            });
        }

        $data = $query->paginate($perPage, ['*'], 'page', $page);        
        
        return view('system.roles.index', [
            'menus' => $menus, 
            'data' => $data, 
            'search'=> $search,                                
        ]);
    }

    public function addRole(Request $request)
    {
        $info = $request->all();
        
        $validator = Validator::make($info, [
            'code' => 'required|unique:roles,code|max:255',
            'name' => 'required|max:255',
            'description' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', __('system.create_role_error'));
        }

        $role = Role::query()
            ->create($info);
        
        return back()->withInput()
            ->with('alert-type', 'alert-success')
            ->with('alert-content', __('system.create_role_success'));
    }


    public function updateRole(Request $request, $id)
    {
        $role = Role::query()->findOrFail($id);

        $info = $request->only(['code', 'name', 'description']);
        $validator = Validator::make($info, [
            'code' => [
                Rule::unique('roles')->ignore($role->id),
                'max:255'
            ],
            'name' => 'required|max:255',
            'description' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', __('system.update_role_error'));
        }

        $role->update($info);

        return back()->withInput()
            ->with('alert-type', 'alert-success')
            ->with('alert-content', __('system.update_success'));
    }

    public function deleteRole($id)
    {        
        Role::destroy($id);

        return back()->withInput()
            ->with('alert-type', 'alert-success')
            ->with('alert-content', __('system.delete_success'));
    }

    public function indexFunctions(Request $request) {
        $user = Auth::user();

        $menus = Menu::query()            
                ->ofRole($user->role_id)
                ->with(['children' => function($query) use($user) {
                    $query->ofRole($user->role_id)
                        ->orderBy('order');
                }, 'roles'])
                ->firstLevel()                        
                ->orderBy('order')
                ->get();
        $search_chuc_nang = $request->get('search_chuc_nang');
        $search_danh_muc = $request->get('search_danh_muc');

        $perPage = $request->get('perpage', 10);
        $page = $request->get('page', 1);
        
        $query = RoleMenu::query()->orderBy('role_id', 'desc');

        if(isset($search_chuc_nang) && is_array($search_chuc_nang)){
            $query->whereIn('role_id', $search_chuc_nang);
        }

        if(isset($search_danh_muc) && is_array($search_danh_muc)){
            $query->whereIn('menu_id', $search_danh_muc);
        }

        $data = $query->paginate($perPage, ['*'], 'page', $page);        
        
        return view('system.functions.index', [
            'menus' => $menus, 
            'data' => $data,          
            'name_role' => Role::query()->get(),   
            'name_menu' => Menu::query()->get(),
            'search_chuc_nang' => $search_chuc_nang,
            'search_danh_muc' => $search_danh_muc
        ]);
    }

    public function deleteFunctions($id)
    {        
        RoleMenu::destroy($id);
        
        return back()->withInput()
            ->with('alert-type', 'alert-success')
            ->with('alert-content', __('system.delete_success'));
    }

    public function addFunctions(Request $request)
    {
        $info = $request->all();  
            
        $rolemenu = RoleMenu::query()
            ->create($info);
        
        return back()->withInput()
            ->with('alert-type', 'alert-success')
            ->with('alert-content', __('system.create_success'));
    }
}
