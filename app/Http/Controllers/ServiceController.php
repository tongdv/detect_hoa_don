<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Menu;
use App\Service;
use App\Role;
use Validator;
use DB;
use Exception;
use App\KeyWord;
use App\StringHelper;
use App\Lookup;

class ServiceController extends Controller
{
    public function index(Request $request) {
        $user = Auth::user();

        $menus = Menu::query()            
                ->ofRole($user->role_id)
                ->with(['children' => function($query) use($user) {
                    $query->ofRole($user->role_id)
                        ->orderBy('order');
                }, 'roles'])
                ->firstLevel()                        
                ->orderBy('order')
                ->get();           

        $perPage = $request->get('perpage', 10);
        $page = $request->get('page', 1);
        $search = $request->get('search');
        
        $query = Service::query();

        if(isset($search)) {
            $query->where(function($query) use($search){
                $query->orWhere('name', 'like', "%{$search}%");
                $query->orWhere('description', 'like', "%{$search}%");                
            });
        }

        $query->orderBy('id', 'desc');
        $query->orderBy('active', 'desc');
        $query->with(['tu_khoas','categoryLookup']);

        $lookups = Lookup::query()
            ->where('active', true)
            ->whereIn('loai', ['category_service'])
            ->get(); 

        $data = $query->paginate($perPage, ['*'], 'page', $page);        
        
        return view('service.index', [
            'menus' => $menus, 
            'data' => $data, 
            'search'=> $search,
            'categories' => $lookups->where('loai','category_service'),                              
        ]);
    }

    public function add(Request $request) {
        // get input
        $info = $request->all();
        // validation
        $validator = Validator::make($info, [
            'name' => 'required|max:255',
            'ratio' => 'numeric|max:100|min:0',
            'active' => 'boolean',
            'key_words' => 'array',
            'key_words.*' => 'max:255|min:1',            
        ]);

        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', 'Lỗi dữ liệu thêm mới service.');                               
        }            
        
        DB::beginTransaction();
        try{        

            $service = Service::query()
                ->create($info);            
            
            if(isset($info['key_words'])) {
                foreach ($info['key_words'] as $value) {  
                    $words = explode (" ", $value);                   
                    // loai bo dau tieng viet phuc vu qua trin nhan dang
                    foreach($words as $key => $word) {                        
                        $words[$key] = StringHelper::vn_to_str($word);
                    }  

                    KeyWord::query()->create(
                        [
                            'tu_khoa' => $value, 
                            'service_id' => $service['id'],
                            'words' => json_encode($words)
                        ]);
                }                
            }
            
            DB::commit();

            return back()->withInput()
                ->with('alert-type', 'alert-success')
                ->with('alert-content', 'Thêm mới dịch vụ thành công.');
        }
        catch(Exception $exception) {
            DB::rollBack();
            return back()->withInput()                
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', 'Có lỗi xảy ra khi thêm mới.' . $exception->getMessage()); 
        }    
    }

    public function delete($id){
        
        DB::beginTransaction();
        try{                             
            KeyWord::query()
                    ->where('service_id', $id)
                    ->delete(); 

            Service::destroy($id);
                                                
            DB::commit();

            return back()->withInput()
                ->with('alert-type', 'alert-success')
                ->with('alert-content', 'Xóa dịch vụ thành công');  
        }
        catch(Exception $exception) {
            DB::rollBack();
            return back()->withInput()                
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', 'Có lỗi xảy ra khi xóa dịch vụ.' . $exception->getMessage()); 
        }         
    }

    public function update(Request $request, $id){
       
        $service = Service::query()->findOrFail($id);
        
        $info = $request->only([ 'name', 'ratio', 'active', 'description', 'key_words', 'category']);                            

        if(empty($info['active'])) {
            $info['active'] = false;
        }
        
        DB::beginTransaction();
        try{        

            $service->update($info);

            if(isset($info['key_words'])) {
                KeyWord::query()
                    ->where('service_id', $service->id)
                    ->delete();
                $keywordUnique = array_unique($info['key_words']);
                
                foreach ($keywordUnique as $value) {
                    $words = explode (" ", $value);                   
                    // loai bo dau tieng viet phuc vu qua trin nhan dang
                    foreach($words as $key => $word) {                        
                        $words[$key] = StringHelper::vn_to_str($word);
                    }

                    KeyWord::query()->create(
                        ['tu_khoa' => $value, 
                        'service_id' => $service['id'],
                        'words' => json_encode($words)
                    ]);
                }                
            }
                                                
            DB::commit();

            return back()->withInput()
                    ->with('alert-type', 'alert-success')
                    ->with('alert-content', 'Cập nhật thông tin thành công.'); 
        }
        catch(Exception $exception) {
            DB::rollBack();
            return back()->withInput()                
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', 'Có lỗi xảy ra khi cập nhật.' . $exception->getMessage()); 
        }             
    }
}
