<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Menu;
use App\Order;
use App\Service;
use Carbon\Carbon;
use DOMDocument;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function sumtotal(Request $request) {
        $user = Auth::user();

        $menus = Menu::query()            
            ->ofRole($user->role_id)
            ->with(['children' => function($query) use($user) {
                $query->ofRole($user->role_id)
                    ->orderBy('order');
            }, 'roles'])
            ->firstLevel()                        
            ->orderBy('order')
            ->get();

        $query = Order::query();
        //$query->whereNotNull('total');
        $perPage = $request->get('perpage', 10);
        $page = $request->get('page', 1);
        $search = $request->get('search'); 
        $search_month = $request->get('search_month');                
        $search_dich_vu = $request->get('search_dich_vu');  
        
        if(isset($search_month)) {
            $search_month = Carbon::createFromFormat(config('datetime.format', 'm/Y'), $search_month);                     
        }
        else{
            $search_month = Carbon::now(); 
        }
        $search_time_start = clone $search_month->startOfMonth();          
        $search_time_end = clone $search_month->endOfMonth();  
        
        $query->where('created_at', '>=', $search_time_start->toDateString());
        $query->where('created_at', '<=', $search_time_end->toDateString());

        if(!empty($search_dich_vu) && is_array($search_dich_vu)){
            $query->whereIn('service_id', $search_dich_vu);
        }  

        if (isset($search)) {
            $query->where(function ($query) use ($search) {
                $query->orWhere('text_detect', 'ilike', "%{$search}%");                
            });
        }  

        $query->orderBy('updated_at','desc');

        if($request->headers->get('accept') =='application/xml') {
            $query->with(['service']);
            $data = $query->get();
            $dom = new DomDocument('1.0');            
            $dom->load("reports/tem_order.xml");            
            $elementKyKhai = $dom->getElementsByTagName('kyKKhai')->item(0);
            $elementKyKhai->nodeValue = $search_month->format('m/Y');

            $elementTuNgay = $dom->getElementsByTagName('kyKKhaiTuNgay')->item(0);
            $elementTuNgay->nodeValue = $search_time_start->format('d/m/Y');

            $elementDenNgay = $dom->getElementsByTagName('kyKKhaiDenNgay')->item(0);
            $elementDenNgay->nodeValue = $search_time_end->format('d/m/Y');

            $elementNgayLap = $dom->getElementsByTagName('ngayLapTKhai')->item(0);
            $elementNgayLap->nodeValue = Carbon::now()->format('d/m/Y');

            $elementNgayKy= $dom->getElementsByTagName('ngayKy')->item(0);
            $elementNgayKy->nodeValue = Carbon::now()->format('d/m/Y');

            $elementsKhaiBao = $dom->getElementsByTagName('CTieuTKhaiChinh')->item(0);
            
            $dataGroup = $data->groupBy('service.category');                       
            $dataSum = array();
            for($i = 21; $i <= 33; $i++) {
                $dataSum[(string)$i] = 0;
            }

            foreach($dataGroup as $key => $groupCategorydata) {                              
                switch($key)  {
                    case 'HHoaXDungCoBaoThauNVL':
                        $dataSum['24'] = $groupCategorydata->sum('total');
                        break;
                    case 'DVuXDungKoBaoThauNVL':
                        $dataSum['26']  = $groupCategorydata->sum('total');
                        break;
                    case 'PPhoiCCapHHoa':
                        $dataSum['22']  = $groupCategorydata->sum('total');
                        break;
                    default:
                        $dataSum['21']  = $groupCategorydata->sum('total');
                }
            }

            $dataSum['23'] = $dataSum['22'] * 0.01;
            $dataSum['25'] = $dataSum['24'] * 0.05;
            $dataSum['27'] = $dataSum['26'] * 0.03;
            $dataSum['29'] = $dataSum['28'] * 0.02;
            $dataSum['30'] = $dataSum['22'] + $dataSum['24'] + $dataSum['26'] + $dataSum['28'];
            $dataSum['31'] = $dataSum['23'] + $dataSum['25'] + $dataSum['27'] + $dataSum['29'];
            $dataSum['32'] = $dataSum['21'] + $dataSum['30'];
            $dataSum['33'] =$dataSum['31'];

            for($i = 21; $i <= 33; $i++) {
                $element= $dom->getElementsByTagName('ct'.$i)->item(0);
                $element->nodeValue = $dataSum[(string)$i];
            }                            
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;                
            return $dom->saveXml();
        }


        $query->with(['service', 'createdBy']);

        $queryDuplicate = clone $query;
        $orders = $queryDuplicate->get();
        $orderGroupByService = $orders->groupBy('service_id');
        foreach ($orders as $key => $order) {
            $order->gtgt = 0;
            if(isset($order->service) && isset($order->service->ratio)) {
                $order->gtgt = $order->service->ratio * $order->total / 100;
            }
        }

        $data =  $query->paginate($perPage, ['*'], 'page', $page);
        foreach ($data as $key => $order) {
            $order->gtgt = 0;
            if(isset($order->service) && isset($order->service->ratio)) {
                $order->gtgt = $order->service->ratio * $order->total / 100;
            }
        }
        
        return view('reports.sumtotal', [
            'menus' =>  $menus,
            'data' => $data,
            'dichvus' => Service::where('active', true)->get(),
            'search_dich_vu' => $search_dich_vu,
            'search_month' => $search_month->format('m/Y'),            
            'sum_total' => $orders->sum('total'), 
            'total_validated' => $orders->where('validator', true)->count(),                            
            'total_input' => $orders->where('input', true)->count(), 
            'total' => $orders->count(),                          
            'total_service' => $orderGroupByService->count(),
            'services' => Service::query()->where('active', true)->get(),
            'sum_gtgt' => $orders->sum('gtgt'),
        ]);
    }

    public function reportInputOutput(Request $request) {
        $user = Auth::user();

        $menus = Menu::query()            
            ->ofRole($user->role_id)
            ->with(['children' => function($query) use($user) {
                $query->ofRole($user->role_id)
                    ->orderBy('order');
            }, 'roles'])
            ->firstLevel()                        
            ->orderBy('order')
            ->get();

        $query = Order::query();
       
        $perPage = $request->get('perpage', 10);
        $page = $request->get('page', 1);
        $search = $request->get('search'); 
        $search_month = $request->get('search_month');                
        $search_dich_vu = $request->get('search_dich_vu');  
        
        if(isset($search_month)) {
            $search_month = Carbon::createFromFormat(config('datetime.format', 'm/Y'), $search_month);                     
        }
        else{
            $search_month = Carbon::now(); 
        }
        $search_time_start = clone $search_month->startOfMonth();          
        $search_time_end = clone $search_month->endOfMonth();  
        
        $query->where('created_at', '>=', $search_time_start->toDateString());
        $query->where('created_at', '<=', $search_time_end->toDateString());

        if(!empty($search_dich_vu) && is_array($search_dich_vu)){
            $query->whereIn('service_id', $search_dich_vu);
        }  

        if (isset($search)) {
            $query->where(function ($query) use ($search) {
                $query->orWhere('text_detect', 'ilike', "%{$search}%");                
            });
        }  

        $query->orderBy('updated_at','desc');

        if($request->headers->get('accept') =='application/xml') {
            $query->with(['service']);
            $data = $query->get();
            $dom = new DomDocument('1.0');            
            $dom->load("reports/tem_order.xml");            
            $elementKyKhai = $dom->getElementsByTagName('kyKKhai')->item(0);
            $elementKyKhai->nodeValue = $search_month->format('m/Y');

            $elementTuNgay = $dom->getElementsByTagName('kyKKhaiTuNgay')->item(0);
            $elementTuNgay->nodeValue = $search_time_start->format('d/m/Y');

            $elementDenNgay = $dom->getElementsByTagName('kyKKhaiDenNgay')->item(0);
            $elementDenNgay->nodeValue = $search_time_end->format('d/m/Y');

            $elementNgayLap = $dom->getElementsByTagName('ngayLapTKhai')->item(0);
            $elementNgayLap->nodeValue = Carbon::now()->format('d/m/Y');

            $elementNgayKy= $dom->getElementsByTagName('ngayKy')->item(0);
            $elementNgayKy->nodeValue = Carbon::now()->format('d/m/Y');

            $elementsKhaiBao = $dom->getElementsByTagName('CTieuTKhaiChinh')->item(0);
            
            $dataGroup = $data->groupBy('service.category');                       
            $dataSum = array();
            for($i = 21; $i <= 33; $i++) {
                $dataSum[(string)$i] = 0;
            }

            foreach($dataGroup as $key => $groupCategorydata) {                              
                switch($key)  {
                    case 'HHoaXDungCoBaoThauNVL':
                        $dataSum['24'] = $groupCategorydata->sum('total');
                        break;
                    case 'DVuXDungKoBaoThauNVL':
                        $dataSum['26']  = $groupCategorydata->sum('total');
                        break;
                    case 'PPhoiCCapHHoa':
                        $dataSum['22']  = $groupCategorydata->sum('total');
                        break;
                    default:
                        $dataSum['21']  = $groupCategorydata->sum('total');
                }
            }

            $dataSum['23'] = $dataSum['22'] * 0.01;
            $dataSum['25'] = $dataSum['24'] * 0.05;
            $dataSum['27'] = $dataSum['26'] * 0.03;
            $dataSum['29'] = $dataSum['28'] * 0.02;
            $dataSum['30'] = $dataSum['22'] + $dataSum['24'] + $dataSum['26'] + $dataSum['28'];
            $dataSum['31'] = $dataSum['23'] + $dataSum['25'] + $dataSum['27'] + $dataSum['29'];
            $dataSum['32'] = $dataSum['21'] + $dataSum['30'];
            $dataSum['33'] =$dataSum['31'];

            for($i = 21; $i <= 33; $i++) {
                $element= $dom->getElementsByTagName('ct'.$i)->item(0);
                $element->nodeValue = $dataSum[(string)$i];
            }                            
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;                
            return $dom->saveXml();
        }


        $query->with(['service', 'createdBy']);

        $queryDuplicate = clone $query;
        $orders = $queryDuplicate->get();
        $orderGroupByService = $orders->groupBy('service_id');
        
        return view('reports.inputoutput.index', [
            'menus' =>  $menus,                      
            'search_dich_vu' => $search_dich_vu,
            'search_month' => $search_month->format('m/Y'),            
            'sum_total' => $orders->sum('total'), 
            'total_validated' => $orders->where('validator', true)->count(),                            
            'total_input' => $orders->where('input', true)->count(), 
            'total' => $orders->count(),
            'sum_input' => $orders->where('input', true)->sum('total'),
            'total_service' => $orderGroupByService->count(),
            'services' => Service::query()->where('active', true)->get(),
        ]);
    }
}
