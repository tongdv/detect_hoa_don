<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Menu;
use App\User;
use App\Role;
use App\Lookup;
use Validator;

class LookupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {   
        $user = Auth::user();

        $menus = Menu::query()            
            ->ofRole($user->role_id)
            ->with(['children' => function($query) use($user) {
                $query->ofRole($user->role_id)
                    ->orderBy('order');
            }, 'roles'])
            ->firstLevel()                        
            ->orderBy('order')
            ->get();
                        

        $query = Lookup::query();

        $perPage = $request->get('perpage', 10);
        $page = $request->get('page', 1);
        $search = $request->get('search');
        $search_loai = $request->get('search_loai');
        $search_trang_thai = $request->get('search_trang_thai');

        if(isset($search_loai)){
        $query->where(function ($query) use ($search_loai) {
            $query->orWhere('loai', $search_loai);
        });
        }

        if(isset($search_trang_thai)){
            $query->where(function ($query) use ($search_trang_thai) {
                $query->orWhere('active',$search_trang_thai);
            });
        }

        if(isset($search)) {           
            $query->where(function($query) use($search){
                $query->orWhere('ma', 'ilike', "%{$search}%");
                $query->orWhere('ten', 'ilike', "%{$search}%");
            });            
        }                        
        $query->orderBy('loai');
        $query->orderBy('ten');
        $query->orderBy('ma');

        $Lookup = $query->paginate($perPage, ['*'], 'page', $page);

        $lookups = Lookup::query()->where('active', true)
            ->whereIn('loai', ['root', 'trang_thai_lookup'])
            ->get();
        return view('lookup.index',[
            'menus' => $menus,
            'data'=> $Lookup,
            'search'=> $search,            
            'loaiLookups' => $lookups->where('loai','root'),
            'actives' => $lookups->where('loai','trang_thai_lookup'),
            'search_trang_thai'=>$search_trang_thai,
            'search_loai'=>$search_loai,
        ]);
    }

    
    public function update(Request $request,$id)
    {             
        $info = $request->only(['ma', 'ten', 'loai', 'active']);    

        $validator = Validator::make($info, [
            'ma' => 'max:255',
            'ten' => 'max:255',
            'loai' => 'max:255',
            'active' => 'boolean',
        ]);

        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', __('system.validator'));
        }     

        if(empty($info['active'])) {
            $info['active'] = false; 
        }

        $lookup= Lookup::findOrFail($id);  

        $lookup->update($info);

        return back()->withInput()
            ->with('alert-type', 'alert-success')
            ->with('alert-content', __('system.update_success'));
    }

    public function delete($id) {
        Lookup::destroy($id);

        return back()->withInput()
            ->with('alert-type', 'alert-success')
            ->with('alert-content', __('system.delete_success'));
    }

    public function add(Request $request){
        $info = $request->all();
       
        $validator = Validator::make($info, [
            'ma' => 'required|max:255',
            'ten' => 'required|max:255',
            'loai' => 'required|max:255',
            'active' => 'boolean',
        ]);

        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('alert-type', 'alert-warning')
                ->with('alert-content', __('system.validator'));
        }              
        
        $lookup = Lookup::query()
            ->create($info);
        
        return back()->withInput()
            ->with('alert-type', 'alert-success')
            ->with('alert-content', __('system.create_success'));      
    }
}
