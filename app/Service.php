<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'ratio', 'active', 'description', 'category'
    ];
    
    /**
     * Get the comments for the blog post.
     */
    public function tu_khoas()
    {
        return $this->hasMany('App\KeyWord');
    }

    public function scopeActive($query)
    {
        return $query->where('active', '=', true);
    }

    public function categoryLookup(){
        return $this->belongsTo('App\Lookup', 'category', 'ma')
            ->where('loai', 'category_service');
    }

    public $timestamps = false;
}
