<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'menu_id', 'home_router'
    ];

    public $timestamps = false;

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function menu(){
        return $this->belongsTo('App\Menu');
    }
}
