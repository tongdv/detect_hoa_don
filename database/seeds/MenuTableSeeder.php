<?php

use Illuminate\Database\Seeder;
use App\Menu;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->delete();

        Menu::create(array(                
            'name' => 'Danh mục', 
            'parent_id' => null,
            'router_link' => '#', 
            'fa_icon' => 'fa-wrench',
            'active' => true,            
            'order' => 1
        ));

        Menu::create(array(                
            'name' => 'Dịch vụ', 
            'parent_id' => 1,
            'router_link' => 'services', 
            'fa_icon' => 'fa-book',
            'active' => true,            
            'order' => 1
        ));

        Menu::create(array(                
            'name' => 'Hệ thống', 
            'parent_id' => null,
            'router_link' => '#', 
            'fa_icon' => 'fa-cog',
            'active' => true,            
            'order' => 2
        ));

        Menu::create(array(                
            'name' => 'Chức năng', 
            'parent_id' => 3,
            'router_link' => 'system.menus', 
            'fa_icon' => 'fa-book',
            'active' => true,            
            'order' => 1
        ));

        Menu::create(array(                
            'name' => 'Quyền', 
            'parent_id' => 3,
            'router_link' => 'system.roles', 
            'fa_icon' => 'fa-book',
            'active' => true,            
            'order' => 2
        ));

        Menu::create(array(                
            'name' => 'Bảng phân quyền', 
            'parent_id' => 3,
            'router_link' => 'system.functions', 
            'fa_icon' => 'fa-book',
            'active' => true,            
            'order' => 3
        ));

        Menu::create(array(                
            'name' => 'Hóa đơn', 
            'parent_id' => null,
            'router_link' => 'orders', 
            'fa_icon' => 'fa-book',
            'active' => true,            
            'order' => 3
        ));
    }
}
