<?php

use Illuminate\Database\Seeder;
use App\Lookup;


class LookupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lookup')->truncate();

        Lookup::create(array(
            'ma' => 'trang_thai_lookup',
            'ten' => 'Trạng thái Lookup',
            'loai' => 'root',
            'active' => true
        ));

        Lookup::create(array(
            'ma' => 'root',
            'ten' => 'Hệ thống',
            'loai' => 'root',
            'active' => true
        ));

        Lookup::create(array(
            'ma' => 1,
            'ten' => 'Đang hoạt động',
            'loai' => 'trang_thai_lookup',
            'active' => true
        ));

        Lookup::create(array(
            'ma' => 0,
            'ten' => 'Ngừng hoạt động',
            'loai' => 'trang_thai_lookup',
            'active' => true
        ));

        Lookup::create(array(
            'ma' => 'key_word_detect_total_order',
            'ten' => 'Từ khóa nhận diện tổng tiền',
            'loai' => 'root',
            'active' => true
        ));

        Lookup::create(array(
            'ma' => 'tong_cong',
            'ten' => 'Tổng cộng',
            'loai' => 'key_word_detect_total_order',
            'active' => true
        ));

        Lookup::create(array(
            'ma' => 'tong_tien',
            'ten' => 'Tổng tiền',
            'loai' => 'key_word_detect_total_order',
            'active' => true
        ));

        Lookup::create(array(
            'ma' => 'tong_thanh_toan',
            'ten' => 'Tổng thanh toán',
            'loai' => 'key_word_detect_total_order',
            'active' => true
        ));

        Lookup::create(array(
            'ma' => 'tong_hoa_don',
            'ten' => 'Tổng hóa đơn',
            'loai' => 'key_word_detect_total_order',
            'active' => true
        ));

        Lookup::create(array(
            'ma' => 'tong_cong_thanh_toan',
            'ten' => 'Tổng cộng thanh toán',
            'loai' => 'key_word_detect_total_order',
            'active' => true
        ));

        Lookup::create(array(
            'ma' => 'cong',
            'ten' => 'Cộng',
            'loai' => 'key_word_detect_total_order',
            'active' => true
        ));
    }
}
