<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->delete();

        Role::create(array(                
            'name' => 'admin',
            'description'    => 'Quản trị hệ thống',
            'code' => 'admin'           
        )); 
        
        Role::create(array(                
            'name' => 'user',
            'description'    => 'Người dùng hệ thống',
            'code' => 'user'           
        ));
    }
}