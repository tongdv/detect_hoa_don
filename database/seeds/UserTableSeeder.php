  <?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();
        
        User::create(array(
            'name'     => 'Admin',
            'username' => 'admin',
            'email'    => 'admin@detectorder.com',
            'password' => Hash::make('12345678'),            
            'role_id' => 1,                                  
            'active' => true
        ));

        User::create(array(
            'name'     => 'Người dùng',
            'username' => 'user',
            'email'    => 'user@detectorder.com',
            'password' => Hash::make('12345678'),            
            'role_id' => 2,
            'active' => true                                    
        ));        
    }
}