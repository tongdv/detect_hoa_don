<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
             RoleTableSeeder::class,
             UserTableSeeder::class,            
             MenuTableSeeder::class,
             RoleMenuTableSeeder::class,
             ServiceTableSeeder::class, 
             LookupSeeder::class,             
        ]);        
    }
}
