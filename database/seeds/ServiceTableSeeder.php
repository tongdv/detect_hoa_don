<?php

use Illuminate\Database\Seeder;
use App\Service;

class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->delete();

        Service::create(array(                
            'name' => 'Phân phối cung cấp hàng hóa', 
            'ratio' => 1,
            'description' => 'Phân phối cung cấp hàng hóa',
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Dịch vụ, xây dựng',
            'description' => 'Dịch vụ, xây dựng không bao thầu nguyên vật liệu',  
            'ratio' => 5,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Dịch vụ cho thuê nhà',
            'description' => 'Dịch vụ cho thuê nhà, đất, cửa hàng, nhà xưởng, cho thuê tài sản, đồ dùng cá nhân khác', 
            'ratio' => 1,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Dịch vụ cho thuê kho bãi',
            'description' => 'Dịch vụ cho thuê kho bãi, máy móc, phương tiện vận tải, …, dịch vụ hỗ trợ liên quan đến vận tải', 
            'ratio' => 1,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Dịch vụ bưu chính, bưu kiện',
            'description' => 'Dịch vụ bưu chính, bưu kiện', 
            'ratio' => 1,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Dịch vụ môi giới',
            'description' => 'Dịch vụ môi giới, đấu giá hoa hồng đại lí', 
            'ratio' => 1,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Dịch vụ tư vấn tài chính',
            'description' => 'Dịch vụ tư vấn tài chính, tư vấn pháp luật', 
            'ratio' => 1,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Dịch vụ làm thủ tục hành chính thuế, hải quan',
            'description' => 'Dịch vụ làm thủ tục hành chính thuế, hải quan', 
            'ratio' => 1,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Dịch vụ công nghệ thông tin',
            'description' => 'Dịch vụ xử lí dữ liệu, cho thuế cổng thông tin, thiết bị viễn thông, công nghệ thông tin', 
            'ratio' => 1,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Dịch vụ hỗ trợ văn phòng, hỗ trợ kinh doanh khác',
            'description' => 'Dịch vụ hỗ trợ văn phòng, hỗ trợ kinh doanh khác', 
            'ratio' => 1,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Dịch vụ tắm hơi, massage, bi – a, internet, games',
            'description' => 'Dịch vụ tắm hơi, massage, bi – a, internet, games', 
            'ratio' => 1,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Dịch vụ may đo giặt là, cắt tóc, gội đầu',
            'description' => 'Dịch vụ may đo giặt là, cắt tóc, gội đầu', 
            'ratio' => 1,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Dịch vụ tư vấn thiết kế, giám sát thi công xây dựng cơ bản',
            'description' => 'Dịch vụ tư vấn thiết kế, giám sát thi công xây dựng cơ bản', 
            'ratio' => 1,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Xây dựng, lắp đặt không bao thầu nguyên vật liệu',
            'description' => 'Xây dựng, lắp đặt không bao thầu nguyên vật liệu', 
            'ratio' => 1,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Sản xuất, vận tải, dịch vụ có gắn với hàng hóa, xây dựng có bao thầu nguyên, vật liệu',
            'description' => 'Sản xuất, vận tải, dịch vụ có gắn với hàng hóa, xây dựng có bao thầu nguyên, vật liệu', 
            'ratio' => 3,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Sản xuất, gia công, chế biến sản phẩm, hàng hóa',
            'description' => 'Sản xuất, gia công, chế biến sản phẩm, hàng hóa', 
            'ratio' => 1,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Khai thác, chế biến khoáng sản',
            'description' => 'Khai thác, chế biến khoáng sản', 
            'ratio' => 1,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Vận tải hàng hóa, vận tải hành khách',
            'description' => 'Vận tải hàng hóa, vận tải hành khách', 
            'ratio' => 1,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Dịch vụ kèm theo',
            'description' => 'Dịch vụ kèm theo', 
            'ratio' => 1,
            'active' => true                     
        ));

        Service::create(array(                
            'name' => 'Hoạt động kinh doanh khác',
            'description' => 'Hoạt động kinh doanh khác', 
            'ratio' => 2,
            'active' => true                     
        ));
    }
}
