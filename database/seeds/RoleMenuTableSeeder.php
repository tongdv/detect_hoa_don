<?php

use Illuminate\Database\Seeder;
use App\RoleMenu;

class RoleMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_menus')->delete();

        RoleMenu::create(array(                
            'role_id' => 1, 
            'menu_id' => 1, 
            'home_router' => false          
        ));

        RoleMenu::create(array(                
            'role_id' => 1, 
            'menu_id' => 2, 
            'home_router' => true          
        ));

        RoleMenu::create(array(                
            'role_id' => 1, 
            'menu_id' => 3,
            'home_router' => false             
        ));        

        RoleMenu::create(array(                
            'role_id' => 1, 
            'menu_id' => 4,
            'home_router' => false             
        )); 
        
        RoleMenu::create(array(                
            'role_id' => 1, 
            'menu_id' => 5,
            'home_router' => false             
        )); 

        RoleMenu::create(array(                
            'role_id' => 1, 
            'menu_id' => 6,
            'home_router' => false             
        ));

        RoleMenu::create(array(                
            'role_id' => 2, 
            'menu_id' => 1, 
            'home_router' => false          
        ));

        RoleMenu::create(array(                
            'role_id' => 2, 
            'menu_id' => 2, 
            'home_router' => true          
        ));

        RoleMenu::create(array(                
            'role_id' => 2, 
            'menu_id' => 3,
            'home_router' => false             
        ));        

        RoleMenu::create(array(                
            'role_id' => 2, 
            'menu_id' => 4,
            'home_router' => false             
        )); 
        
        RoleMenu::create(array(                
            'role_id' => 2, 
            'menu_id' => 5,
            'home_router' => false             
        )); 

        RoleMenu::create(array(                
            'role_id' => 2, 
            'menu_id' => 6,
            'home_router' => false             
        ));

        RoleMenu::create(array(                
            'role_id' => 1, 
            'menu_id' => 7,
            'home_router' => false             
        ));

        RoleMenu::create(array(                
            'role_id' => 2, 
            'menu_id' => 7,
            'home_router' => false             
        ));
    }
}
