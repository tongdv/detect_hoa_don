<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('profile', 'ProfileController@showProfileForm')->name('profile');
Route::post('profile', 'ProfileController@update')->name('profile.update');
Route::post('changepassword', 'ProfileController@changePassword')->name('profile.changepassword');

Route::get('users', 'UserController@index')->name('users');
Route::get('users/show/{id}', 'UserController@show')->name('users.show');
Route::post('users/update/{id}', 'UserController@update')->name('users.update');
Route::delete('users/delete/{id}', 'UserController@delete')->name('users.delete');
Route::post('users/add', 'UserController@add')->name('users.add');

Route::get('roles', 'SystemController@indexRole')->name('system.roles');
Route::post('roles/add', 'SystemController@addRole')->name('system.roles.add');
Route::put('roles/update/{id}', 'SystemController@updateRole')->name('system.roles.update');
Route::delete('roles/delete/{id}', 'SystemController@deleteRole')->name('system.roles.delete');

Route::get('menus', 'SystemController@indexMenu')->name('system.menus');
Route::post('menus/add', 'SystemController@addMenu')->name('system.menus.add');
Route::put('menus/update/{id}', 'SystemController@updateMenu')->name('system.menus.update');
Route::delete('menus/delete/{id}', 'SystemController@deleteMenu')->name('system.menus.delete');

Route::get('lookup','LookupController@index')->name('lookup');
Route::post('lookup/{id}', 'LookupController@update')->name('lookup.update');
Route::delete('lookup/delete/{id}', 'LookupController@delete')->name('lookup.delete');
Route::post('lookup', 'LookupController@add')->name('lookup.add');

Route::get('functions', 'SystemController@indexFunctions')->name('system.functions');
Route::post('functions/add', 'SystemController@addFunctions')->name('system.functions.add');
Route::delete('functions/delete/{id}', 'SystemController@deleteFunctions')->name('system.functions.delete');

Route::get('services', 'ServiceController@index')->name('services');
Route::post('services/add', 'ServiceController@add')->name('services.add');
Route::put('services/update/{id}', 'ServiceController@update')->name('services.update');
Route::delete('services/delete/{id}', 'ServiceController@delete')->name('services.delete');

Route::get('orders', 'OrderController@index')->name('orders');
Route::post('orders/addByDetect', 'OrderController@addByDetect')->name('orders.addByDetect');
Route::put('orders/update/{id}', 'OrderController@update')->name('orders.update');
Route::delete('orders/delete/{id}', 'OrderController@delete')->name('orders.delete');

Route::get('report/sumtotal', 'ReportController@sumtotal')->name('report.sumtotal');
Route::get('report/inputoutput', 'ReportController@reportInputOutput')->name('report.inputOutput');



